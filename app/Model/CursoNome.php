<?php
/**
 * Curso
 *
 * PHP version 5
 *
 * @category Model
 * @package  Exame IFAL
 * @version  1.0
 * @author   Uziel Barbosa <contato@uzielbarbosa.com.br>
 * @license  http://www.opensource.org/licenses/mit-license.php The MIT License
 * @link     http://www.croogo.org
 */
class CursoNome extends AppModel {

/**
 * Model name
 *
 * @var string
 * @access public
 */
	public $name = 'CursoNome';
        
        public $useTable = 'cursos_nomes';

        public $belongsTo = array(
                'TipoCurso' => array(
			'className' => 'TipoCurso',
			'foreignKey' => 'tipo_curso_id',
			'conditions' => '',
			'fields' => '',
			'order' => '',
		),
        );


/**
 * Validation
 *
 * @var array
 * @access public
 */
	public $validate = array(
                'nome' => array(
			'notEmpty' => array(
				'rule' => 'notEmpty',
				'message' => 'O campo deve ser informado.',
			),
		),
                'tipo_curso_id' => array(
			'notEmpty' => array(
				'rule' => 'notEmpty',
				'message' => 'O campo deve ser informado.',
			),
		),
                
	);
        

}
