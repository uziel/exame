<?php
/**
 * Exame Controller
 *
 * PHP version 5
 *
 * @category Controller
 * @package  Exame IFAL
 * @version  1.0
 * @author   Uziel Barbosa <contato@uzielbarbosa.com.br>
 * @license  http://www.opensource.org/licenses/mit-license.php The MIT License
 * @link     http://exame.ifal.edu.br
 */
class ExamesController extends AppController {

/**
 * Controller name
 *
 * @var string
 * @access public
 */
	public $name = 'Exames';

/**
 * Models used by the Controller
 *
 * @var array
 * @access public
 */
	public $uses = array('Exame','ExameCurso','Curso','CursoDescricao');

	public function admin_index() {
		$this->set('title_for_layout', __('Exames'));

		$this->Exame->recursive = 0;
		$this->paginate['Exame']['order'] = "Exame.id ASC";
		$this->set('exames', $this->paginate());
	}
        
        
        public function admin_painel($id  = null) {
            
            if(!$id) {
                $this->Session->setFlash(__('O exame não foi selecionado'), 'default', array('class' => 'error'));
                $this->redirect(array('action' => 'index'));
            }
            
            $this->Exame->recursive = 0;
            $exame = $this->Exame->read(null, $id);
            
            $this->set('title_for_layout', $exame['Exame']['nome']);
            
            $this->set(compact('exame'));
	}

	public function admin_add() {
		$this->set('title_for_layout', __('Adicionar Exame'));

		if (!empty($this->request->data)) {
			$this->Exame->create();
                        
                        $this->request->data['Exame']['data_cadastro'] = date('Y-m-d H:i:s');
                        $this->request->data['Exame']['user_id'] = $this->Session->read('Auth.User.id');
                        
                        $this->request->data['Exame']['codigo'] = '004100112233';
                        
			if ($this->Exame->save($this->request->data)) {
				$this->Session->setFlash(__('Exame cadastrado com sucesso'), 'default', array('class' => 'success'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Erro ao cadastrar exame, tente novamente'), 'default', array('class' => 'error'));
                                //pr($this->Exame->invalidFields()); break;
			}
		}
                
                $this->carregarDadosCombobox('TipoExame','tiposExames');
                $this->carregarDadosCombobox('LinguaEstrangeira','linguas');
                
	}
        
        public function admin_ofertas($exameId = null) {
		$this->set('title_for_layout', __('Ofertar Curso'));
                
                if(!$exameId) {
                    $this->Session->setFlash(__('Exame inválido'), 'default', array('class' => 'error'));
                    $this->redirect(array('action' => 'index'));
                }

		if (!empty($this->request->data)) {
                    
			$this->ExameCurso->create();
                        
                        $this->request->data['ExameCurso']['data_cadastro'] = date('Y-m-d H:i:s');
                        $this->request->data['ExameCurso']['user_id'] = $this->Session->read('Auth.User.id');
                        
                        $this->request->data['ExameCurso']['codigo'] = '004100112233';
                        
			if ($this->ExameCurso->save($this->request->data)) {
				$this->Session->setFlash(__('Oferta cadastrada com sucesso'), 'default', array('class' => 'success'));
				$this->redirect(array('action' => 'ofertas', $this->request->data['ExameCurso']['exame_id']));
			} else {
				$this->Session->setFlash(__('Erro ao cadastrar oferta, tente novamente'), 'default', array('class' => 'error'));
			}
		}
                
                $cursos = $this->Curso->find('all', array('fields' => array('id', 'CursoDescricao.descricao')));
                
                $this->ExameCurso->recursive = 2;
                $condicoes = array('ExameCurso.exame_id' => $exameId);
                $exameCursos = $this->ExameCurso->find('all', array('conditions' => $condicoes));
                
                $this->set(compact('exameId','cursos','exameCursos'));
                
	}

	public function admin_edit($id = null) {
		$this->set('title_for_layout', __('Editar Exame'));

		if (!$id && empty($this->request->data)) {
			$this->Session->setFlash(__('Exame inválida'), 'default', array('class' => 'error'));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->request->data)) {
			if ($this->Exame->save($this->request->data)) {
				$this->Session->setFlash(__('Exame alterado com sucesso'), 'default', array('class' => 'success'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Erro ao alterar campus, tente novamente'), 'default', array('class' => 'error'));
			}
		}
		if (empty($this->request->data)) {
			$this->request->data = $this->Exame->read(null, $id);
                        App::uses('Util', 'Vendor');
                        $this->request->data['Exame']['data_inicio_inscricao_normal'] = Util::converteData($this->request->data['Exame']['data_inicio_inscricao_normal']);            
                        $this->request->data['Exame']['data_fim_inscricao_normal'] = Util::converteData($this->request->data['Exame']['data_fim_inscricao_normal']);            
                        $this->request->data['Exame']['data_inicio_inscricao_isento'] = Util::converteData($this->request->data['Exame']['data_inicio_inscricao_isento']);            
                        $this->request->data['Exame']['data_fim_inscricao_isento'] = Util::converteData($this->request->data['Exame']['data_fim_inscricao_isento']);            
                        $this->request->data['Exame']['data_exame'] = Util::converteData($this->request->data['Exame']['data_exame']);            
                        $this->set(compact('campus'));
		}
	}

	public function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Id do Exame inválido'), 'default', array('class' => 'error'));
			$this->redirect(array('action' => 'index'));
		}
		if ($this->Exame->delete($id)) {
			$this->Session->setFlash(__('Exame excluído com sucesso'), 'default', array('class' => 'success'));
			$this->redirect(array('action' => 'index'));
		}
	}

}
