<?php
/**
 * ExameCurso
 *
 * PHP version 5
 *
 * @category Model
 * @package  ExameCurso IFAL
 * @version  1.0
 * @author   Uziel Barbosa <contato@uzielbarbosa.com.br>
 * @license  http://www.opensource.org/licenses/mit-license.php The MIT License
 * @link     http://www.croogo.org
 */
class ExameCurso extends AppModel {

/**
 * Model name
 *
 * @var string
 * @access public
 */
	public $name = 'ExameCurso';
        
        public $useTable = 'exames_cursos';



/**
 * Validation
 *
 * @var array
 * @access public
 */
	public $validate = array(
                'exame_id' => array(
			'notEmpty' => array(
				'rule' => 'notEmpty',
				'message' => 'O campo deve ser informado.',
			),
		),
                'curso_id' => array(
			'notEmpty' => array(
				'rule' => 'notEmpty',
				'message' => 'O campo deve ser informado.',
			),
		),
                'numero_vagas' => array(
			'notEmpty' => array(
				'rule' => 'notEmpty',
				'message' => 'O campo deve ser informado.',
			),
		),
               
	);

        
        public $belongsTo = array(
		'Exame' => array(
			'className' => 'Exame',
			'foreignKey' => 'exame_id',
			'conditions' => '',
			'fields' => '',
			'order' => '',
		),
                'Curso' => array(
			'className' => 'Curso',
			'foreignKey' => 'curso_id',
			'conditions' => '',
			'fields' => '',
			'order' => '',
		),
	);

}
