<?php
/**
 * NivelCurso
 *
 * PHP version 5
 *
 * @category Model
 * @package  Exame IFAL
 * @version  1.0
 * @author   Uziel Barbosa <contato@uzielbarbosa.com.br>
 * @license  http://www.opensource.org/licenses/mit-license.php The MIT License
 * @link     http://www.croogo.org
 */
class NivelCurso extends AppModel {

/**
 * Model name
 *
 * @var string
 * @access public
 */
	public $name = 'NivelCurso';
        
        public $useTable = 'niveis_cursos';

/**
 * Validation
 *
 * @var array
 * @access public
 */
	public $validate = array(
                'descricao' => array(
			'notEmpty' => array(
				'rule' => 'notEmpty',
				'message' => 'O campo deve ser informado.',
			),
		),
                'codigo' => array(
			'isUnique' => array(
				'rule' => 'isUnique',
				'message' => 'Nivel de curso já cadastrado com esse código',
			),
			'notEmpty' => array(
				'rule' => 'notEmpty',
				'message' => 'O campo deve ser informado.',
			),
		),
	);

/**
 * Display fields for this model
 *
 * @var array
 */
	protected $_displayFields = array(
		'id',
		'descricao',
                'codigo',
	);

}
