<?php
/**
 * NivelCursos Controller
 *
 * PHP version 5
 *
 * @category Controller
 * @package  Exame IFAL
 * @version  1.0
 * @author   Uziel Barbosa <contato@uzielbarbosa.com.br>
 * @license  http://www.opensource.org/licenses/mit-license.php The MIT License
 * @link     http://exame.ifal.edu.br
 */
class NiveisCursosController extends AppController {

/**
 * Controller name
 *
 * @var string
 * @access public
 */
	public $name = 'NiveisCursos';

/**
 * Models used by the Controller
 *
 * @var array
 * @access public
 */
	public $uses = array('NivelCurso');

	public function admin_index() {
		$this->set('title_for_layout', __('Níveis de Cursos'));

		$this->NivelCurso->recursive = 0;
		$this->paginate['NivelCurso']['order'] = "NivelCurso.codigo ASC";
		$this->set('niveis', $this->paginate());
		$this->set('displayFields', $this->NivelCurso->displayFields());
	}

	public function admin_add() {
		$this->set('title_for_layout', __('Adicionar Nível de Curso'));

		if (!empty($this->request->data)) {
			$this->NivelCurso->create();
			if ($this->NivelCurso->save($this->request->data)) {
				$this->Session->setFlash(__('Nível de Curso cadastrado com sucesso'), 'default', array('class' => 'success'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Erro ao cadastrar nível de curso, tente novamente'), 'default', array('class' => 'error'));
			}
		}
	}

	public function admin_edit($id = null) {
		$this->set('title_for_layout', __('Editar Nível de Curso'));

		if (!$id && empty($this->request->data)) {
			$this->Session->setFlash(__('Nível de Curso inválido'), 'default', array('class' => 'error'));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->request->data)) {
			if ($this->NivelCurso->save($this->request->data)) {
				$this->Session->setFlash(__('Nível de Curso alterado com sucesso'), 'default', array('class' => 'success'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Erro ao alterar nível de curso, tente novamente'), 'default', array('class' => 'error'));
			}
		}
		if (empty($this->request->data)) {
			$this->request->data = $this->NivelCurso->read(null, $id);
		}
	}

	public function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Id do Nível de Curso inválido'), 'default', array('class' => 'error'));
			$this->redirect(array('action' => 'index'));
		}
		if ($this->NivelCurso->delete($id)) {
			$this->Session->setFlash(__('Nível de Curso excluído com sucesso'), 'default', array('class' => 'success'));
			$this->redirect(array('action' => 'index'));
		}
	}

}
