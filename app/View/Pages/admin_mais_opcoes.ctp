<div class="dashboard simplebox">
    <div class="title">
        <?php echo $title_for_layout; ?>
        <span></span>
    </div>
    
    <div class="body">
        <div class="actions">
            <ul>
                <li><?php echo $this->Html->link(__('Etnias', true), array('controller' => 'etnias','action'=>'index')); ?></li>
                <li><?php echo $this->Html->link(__('Habilitações', true), array('controller' => 'habilitacoes','action'=>'index')); ?></li>
                <li><?php echo $this->Html->link(__('Necessidades Especiais', true), array('controller' => 'necessidades_especiais','action'=>'index')); ?></li>
                <li><?php echo $this->Html->link(__('Níveis de Cursos', true), array('controller' => 'niveis_cursos','action'=>'index')); ?></li>
                <li><?php echo $this->Html->link(__('Eixos de Cursos', true), array('controller' => 'eixos_cursos','action'=>'index')); ?></li>
                <li><?php echo $this->Html->link(__('Modalidades de Cursos', true), array('controller' => 'modalidades_cursos','action'=>'index')); ?></li>
                <li><?php echo $this->Html->link(__('Áreas de Cursos', true), array('controller' => 'areas_cursos','action'=>'index')); ?></li>
                <li><?php echo $this->Html->link(__('Turnos', true), array('controller' => 'turnos','action'=>'index')); ?></li>
                <li><?php echo $this->Html->link(__('Descrição de Cursos', true), array('controller' => 'cursos_descricoes','action'=>'index')); ?></li>
            </ul>
        </div>
    </div>
</div>