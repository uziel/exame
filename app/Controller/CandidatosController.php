<?php
/**
 * Users Controller
 *
 * PHP version 5
 *
 * @category Controller
 * @package  Croogo
 * @version  1.0
 * @author   Fahad Ibnay Heylaal <contact@fahad19.com>
 * @license  http://www.opensource.org/licenses/mit-license.php The MIT License
 * @link     http://www.croogo.org
 */
class CandidatosController extends AppController {

/**
 * Controller name
 *
 * @var string
 * @access public
 */
	public $name = 'Candidatos';

/**
 * Components
 *
 * @var array
 * @access public
 */
	public $components = array(
		'Email',
	);

/**
 * Models used by the Controller
 *
 * @var array
 * @access public
 */
	public $uses = array('Candidato');

	public function admin_index() {
		$this->set('title_for_layout', __('Users'));

		$this->User->recursive = 0;
		$this->set('users', $this->paginate());
		$this->set('displayFields', $this->User->displayFields());
	}

	public function admin_add() {
		if (!empty($this->request->data)) {
			$this->User->create();
			$this->request->data['User']['activation_key'] = md5(uniqid());
			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash(__('The User has been saved'), 'default', array('class' => 'success'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The User could not be saved. Please, try again.'), 'default', array('class' => 'error'));
				unset($this->request->data['User']['password']);
			}
		} else {
			$this->request->data['User']['role_id'] = 2; // default Role: Registered
		}
		$roles = $this->User->Role->find('list');
		$this->set(compact('roles'));
	}

/**
 * admin_edit
 *
 * @param integer $id
 */
	public function admin_edit($id = null) {
		if (!empty($this->request->data)) {
			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash(__('The User has been saved'), 'default', array('class' => 'success'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The User could not be saved. Please, try again.'), 'default', array('class' => 'error'));
			}
		} else {
			$this->request->data = $this->User->read(null, $id);
		}
		$roles = $this->User->Role->find('list');
		$this->set(compact('roles'));
		$this->set('editFields', $this->User->editFields());
	}

	public function admin_reset_password($id = null) {
		if (!$id && empty($this->request->data)) {
			$this->Session->setFlash(__('Invalid User'), 'default', array('class' => 'error'));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->request->data)) {
			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash(__('Password has been reset.'), 'default', array('class' => 'success'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Password could not be reset. Please, try again.'), 'default', array('class' => 'error'));
			}
		}
		if (empty($this->request->data)) {
			$this->request->data = $this->User->read(null, $id);
		}
	}

	public function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for User'), 'default', array('class' => 'error'));
			$this->redirect(array('action' => 'index'));
		}
		if ($this->User->delete($id)) {
			$this->Session->setFlash(__('User deleted'), 'default', array('class' => 'success'));
			$this->redirect(array('action' => 'index'));
		} else {
			$this->Session->setFlash(__('User cannot be deleted'), 'default', array('class' => 'error'));
			$this->redirect(array('action' => 'index'));
		}
	}

	public function add() {
		$this->set('title_for_layout', __('Cadastro do Candidato'));
                App::import('Vendor', 'Util', array('file' => 'util.php'));
		if (!empty($this->request->data)) {
                    
			$this->Candidato->create();
			$this->request->data['User']['role_id'] = 2; // Registered
			$this->request->data['User']['activation_key'] = md5(uniqid());
			$this->request->data['User']['status'] = 1;
			$this->request->data['User']['username'] = htmlspecialchars($this->request->data['User']['email']);
			$this->request->data['User']['name'] = htmlspecialchars($this->request->data['Candidato']['nome']);
                        $this->request->data['Candidato']['data_nascimento'] = Util::converteData($this->request->data['Candidato']['data_nascimento']);
			if ($this->Candidato->saveAll($this->request->data)) {
				$this->Session->setFlash(__('Cadastro realizado com sucesso.'), 'default', array('class' => 'success'));
				$this->redirect(array('action' => 'login'));
			} else {
				Croogo::dispatchEvent('Controller.Users.registrationFailure', $this);
                                $this->request->data['Candidato']['data_nascimento'] = Util::converteData($this->request->data['Candidato']['data_nascimento']);
				$this->Session->setFlash(__('Erro ao realizar cadastro, verifique os erros ocorridos e tente novamente.'), 'default', array('class' => 'error'));
			}
		}
                
                $this->carregarDadosCombobox('Etnia','etnias');
                
	}
        
        
}
