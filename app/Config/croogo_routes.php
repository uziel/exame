<?php
	CakePlugin::routes();
	Router::parseExtensions('json', 'rss');

	// Basic
	CroogoRouter::connect('/', array('controller' => 'pages', 'action' => 'index'));
	CroogoRouter::connect('/admin', array('admin' => true, 'controller' => 'pages', 'action' => 'dashboard'));
        CroogoRouter::connect('/admin/pages', array('admin' => true, 'controller' => 'pages', 'action' => 'dashboard'));

	// Page
	CroogoRouter::connect('/about', array('controller' => 'nodes', 'action' => 'view', 'type' => 'page', 'slug' => 'about'));
	CroogoRouter::connect('/page/:slug', array('controller' => 'nodes', 'action' => 'view', 'type' => 'page'));

	// Users
	CroogoRouter::connect('/cadastro', array('controller' => 'candidatos', 'action' => 'add'));
	CroogoRouter::connect('/user/:username', array('controller' => 'users', 'action' => 'view'), array('pass' => array('username')));

	// Contact
	CroogoRouter::connect('/contact', array('controller' => 'contacts', 'action' => 'view', 'contact'));
