<?php //$this->extend('/Common/admin_index'); ?>

<div class="habilitacoes index">
	<h2><?php echo $title_for_layout; ?></h2>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('Nova Habilitação', true), array('action'=>'add')); ?></li>
		</ul>
	</div>

	<table cellpadding="0" cellspacing="0">
	<?php
		$tableHeaders =  $this->Html->tableHeaders(array(
			$this->Paginator->sort('id'),
			$this->Paginator->sort('descricao','Descrição'),
			__('Ações', true),
		));
		echo $tableHeaders;

		$rows = array();
		foreach ($habilitacoes AS $habilitacao) {
			$actions  = $this->Html->link(__('Editar', true), array('controller' => 'habilitacoes', 'action' => 'edit', $habilitacao['Habilitacao']['id']));
			$actions .= ' ' . $this->Layout->adminRowActions($habilitacao['Habilitacao']['id']);
			$actions .= ' ' . $this->Html->link(__('Excluir', true), array(
				'controller' => 'habilitacoes',
				'action' => 'delete',
				$habilitacao['Habilitacao']['id'],
				'token' => $this->params['_Token']['key'],
			), null, __('Are you sure?', true));

			$rows[] = array(
				$habilitacao['Habilitacao']['id'],
				h($habilitacao['Habilitacao']['descricao']),
				$actions,
			);
		}

		echo $this->Html->tableCells($rows);
		echo $tableHeaders;
	?>
	</table>
</div>

<div class="paging"><?php echo $this->Paginator->numbers(); ?></div>
<div class="counter"><?php echo $this->Paginator->counter(array('format' => __('Página %page% de %pages%, mostrando %current% registro de %count% total'))); ?></div>

