<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Sistema de Exames - <?php echo $title_for_layout; ?></title>
    <?php
        echo $this->Html->css(array(
            'reset',
            '/ui-themes/smoothness/jquery-ui.css',
            'root',
            'admin',
            //'thickbox',
        ));
        echo $this->Layout->js();
        echo $this->Html->script(array(
            'jquery/jquery.min',
            'jquery/jquery-ui.min',
            'jquery/jquery.slug',
            #'jquery/jquery.uuid',
            'jquery/jquery.cookie',
            'jquery/jquery.hoverIntent.minified',
            'jquery/superfish',
            'jquery/supersubs',
            'jquery/jquery.tipsy',
            'jquery/jquery.elastic-1.6.1.js',
            'jquery/thickbox-compressed',
            'admin',
        ));
        echo $scripts_for_layout;
    ?>
</head>

<body>
    

    <div class="wrapper">
        
        <div id="header">
            <?php echo $this->element('admin/header'); ?>
        </div>
        
        <div class="quick-menu">
            <?php echo $this->element("admin/quick-menu"); ?>
        </div>
        
        <div id="content">
            
            <div class="page-wrap">
                <?php echo $this->Breadcrumb->get();?>
            </div>
            <div class="main">
                <div class="mensagens">
                    <?php
                        echo $this->Layout->sessionFlash();
                    ?>
                </div>
                <?php
                    echo $content_for_layout;
                ?>
            </div>
        </div>
        
    </div>

    </body>
</html>