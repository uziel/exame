<?php
/**
 * Aviso
 *
 * PHP version 5
 *
 * @category Model
 * @package  Exame IFAL
 * @version  1.0
 * @author   Uziel Barbosa <contato@uzielbarbosa.com.br>
 * @license  http://www.opensource.org/licenses/mit-license.php The MIT License
 * @link     http://www.croogo.org
 */
class Aviso extends AppModel {

/**
 * Model name
 *
 * @var string
 * @access public
 */
	public $name = 'Aviso';
        
        public $useTable = 'avisos';



/**
 * Validation
 *
 * @var array
 * @access public
 */
	public $validate = array(
                'titulo' => array(
			'rule' => 'notEmpty',
			'message' => 'O campo deve ser informado.',
		),
                'texto' => array(
			'rule' => 'notEmpty',
			'message' => 'O campo deve ser informado.',
		),
	);

/**
 * Display fields for this model
 *
 * @var array
 */
	protected $_displayFields = array(
		'id',
		'titulo' => 'Título',
                'texto',
	);
        
        
        
   public $belongsTo = array(
		'Exame' => array(
			'className' => 'Exame',
			'foreignKey' => 'exame_id',
			'conditions' => '',
			'fields' => '',
			'order' => '',
		),
	);

}
