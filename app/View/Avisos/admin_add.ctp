<?php $this->Html->script(array('jquery/jquery.maskedinput'), false); ?>

<?php $this->extend('/Common/admin_edit'); ?>
<?php echo $this->Form->create('Aviso');?>

    <?php
            echo $this->Form->input('exame_id', array('value' => $exameId));
            echo $this->Form->input('titulo', array ('label' => 'Nome', 'class' => 'input-big'));
            echo $this->Form->input('texto', array ('label' => 'Descrição*', 'class' => 'input-big'));
    ?>
		
    <div class="clear"></div>
    
    <div class="buttons">
    <?php
            echo $this->Form->end(__('Salvar'));
            echo $this->Html->link(__('Cancelar'), array(
                    'action' => 'index',$exameId,
            ), array(
                    'class' => 'cancel',
            ));
    ?>
    </div>
    </div>
</div>


<style type="text/css">
    .input {
        min-height: 71px;
    }
</style>


<script type="text/javascript">
    
    jQuery(function($){
       $("#EnderecoCep").mask("99999-999");
       
       $("#ExameDataInicioInscricaoNormal").mask("99/99/9999").datepicker({dateFormat: "dd/mm/yy"});
       $("#ExameDataFimInscricaoNormal").mask("99/99/9999").datepicker({dateFormat: "dd/mm/yy"});
       $("#ExameDataInicioInscricaoIsento").mask("99/99/9999").datepicker({dateFormat: "dd/mm/yy"});
       $("#ExameDataFimInscricaoIsento").mask("99/99/9999").datepicker({dateFormat: "dd/mm/yy"});
       $("#ExameDataExame").mask("99/99/9999").datepicker({dateFormat: "dd/mm/yy"});
       
    });
    
</script>