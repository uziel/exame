<?php //$this->extend('/Common/admin_index'); ?>

<div class="campus index simplebox">
	
        <div class="title">
            <?php echo $title_for_layout; ?>
            <span></span>
        </div>
    
        <div class="body">

            <div class="actions">
                    <ul>
                            <li><?php echo $this->Html->link(__('Novo Campus', true), array('action'=>'add')); ?></li>
                    </ul>
            </div>

            <table cellpadding="0" cellspacing="0">
            <?php
                    $tableHeaders =  $this->Html->tableHeaders(array(
                            $this->Paginator->sort('codigo','Código'),  
                            $this->Paginator->sort('descricao','Descrição'),
                            __('Ações', true),
                    ));
                    echo $tableHeaders;

                    $rows = array();
                    foreach ($campus AS $c) {
                            $actions  = $this->Html->link(__('Editar', true), array('controller' => 'campus', 'action' => 'edit', $c['Campus']['id']));
                            $actions .= ' ' . $this->Layout->adminRowActions($c['Campus']['id']);
                            $actions .= ' ' . $this->Html->link(__('Excluir', true), array(
                                    'controller' => 'campus',
                                    'action' => 'delete',
                                    $c['Campus']['id'],
                                    'token' => $this->params['_Token']['key'],
                            ), null, __('Are you sure?', true));

                            $rows[] = array(
                                    h($c['Campus']['codigo']),
                                    h($c['Campus']['descricao']),
                                    $actions,
                            );
                    }

                    echo $this->Html->tableCells($rows);
            ?>
            <div class="paging">
            <?php echo $this->Paginator->prev('');?>
            <?php echo $this->Paginator->numbers(array('separator' => '')); ?>
            <?php echo $this->Paginator->next('');?>
        </div>
    </div>
</div>