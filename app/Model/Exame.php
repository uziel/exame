<?php
/**
 * Exame
 *
 * PHP version 5
 *
 * @category Model
 * @package  Exame IFAL
 * @version  1.0
 * @author   Uziel Barbosa <contato@uzielbarbosa.com.br>
 * @license  http://www.opensource.org/licenses/mit-license.php The MIT License
 * @link     http://www.croogo.org
 */
class Exame extends AppModel {

/**
 * Model name
 *
 * @var string
 * @access public
 */
	public $name = 'Exame';



/**
 * Validation
 *
 * @var array
 * @access public
 */
	public $validate = array(
                'nome' => array(
			'rule' => 'notEmpty',
			'message' => 'O campo deve ser informado.',
		),
                'descricao' => array(
			'notEmpty' => array(
				'rule' => 'notEmpty',
				'message' => 'O campo deve ser informado.',
			),
		),
                'taxa_inscricao' => array(
			'rule' => 'notEmpty',
			'message' => 'O campo deve ser informado.',
		),
                'data_inicio_inscricao_normal' => array(
			'notEmpty' => array(
				'rule' => 'notEmpty',
				'message' => 'O campo deve ser informado.',
			),
		),
                'data_fim_inscricao_normal' => array(
			'notEmpty' => array(
				'rule' => 'notEmpty',
				'message' => 'O campo deve ser informado.',
			),
		),
                'data_inicio_inscricao_isento' => array(
			'notEmpty' => array(
				'rule' => 'notEmpty',
				'message' => 'O campo deve ser informado.',
			),
		),
                'data_fim_inscricao_isento' => array(
			'notEmpty' => array(
				'rule' => 'notEmpty',
				'message' => 'O campo deve ser informado.',
			),
		),
                'data_exame' => array(
			'notEmpty' => array(
				'rule' => 'notEmpty',
				'message' => 'O campo deve ser informado.',
			),
		),
	);
        
        
        
        public function beforeSave($options = array()) {
            
            if(!empty($this->data['Exame']['data_inicio_inscricao_normal']) && 
               !empty($this->data['Exame']['data_fim_inscricao_normal']) && 
               !empty($this->data['Exame']['data_inicio_inscricao_isento']) && 
               !empty($this->data['Exame']['data_fim_inscricao_isento']) && 
               !empty($this->data['Exame']['data_exame'])) { 
                
                App::uses('Util', 'Vendor');
                $this->data['Exame']['data_inicio_inscricao_normal'] = Util::converteData($this->data['Exame']['data_inicio_inscricao_normal']);            
                $this->data['Exame']['data_fim_inscricao_normal'] = Util::converteData($this->data['Exame']['data_fim_inscricao_normal']);            
                $this->data['Exame']['data_inicio_inscricao_isento'] = Util::converteData($this->data['Exame']['data_inicio_inscricao_isento']);            
                $this->data['Exame']['data_fim_inscricao_isento'] = Util::converteData($this->data['Exame']['data_fim_inscricao_isento']);            
                $this->data['Exame']['data_exame'] = Util::converteData($this->data['Exame']['data_exame']);            
            }     
            
            return true;
            
        }

       

       

}
