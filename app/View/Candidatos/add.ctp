<?php $this->Html->script(array('jquery/jquery.maskedinput'), false); ?>
<div class="cadastro-candidato form">
    <h2><?php echo $title_for_layout; ?></h2>
    <?php echo $this->Form->create('Candidato'); ?>
    <fieldset>
        <legend>Dados pessoais</legend>
        <?php
            echo $this->Form->input('nome', array('label' => 'Nome*', 'class' => 'input-xxlarge'));
            echo $this->Form->input('nome_pai', array('label' => 'Nome do pai', 'class' => 'input-xxlarge'));
            echo $this->Form->input('nome_mae', array('label' => 'Nome da mãe*', 'class' => 'input-xxlarge'));
            echo $this->Form->input('data_nascimento', array(
                'label' => 'Data de nascimento*',
                'type' => 'text'
            ));
            echo $this->Form->input('cpf', array(
                'label' => 'CPF*', 
                'div' => array('class' => 'input text input-before')
            ));
            echo $this->Form->input('rg', array('label' => 'RG*'));
            echo $this->Form->input('telefone_1', array(
                'label' => 'Telefone 1*', 
                'div' => array('class' => 'input text input-before')
            ));
            echo $this->Form->input('telefone_2', array('label' => 'Telefone 2'));
            echo $this->Form->input('etnia_id', array('label' => 'Etnia*','empty' => 'Selecione...'));
        ?>
    </fieldset>
    <fieldset>
        <legend>Dados de endereço</legend>
        <?php
            echo $this->Form->input('Endereco.cep',array('label' => 'CEP*'));
            echo $this->Form->input('Endereco.logradouro', array(
                'label' => 'Endereço*', 
                'class' => 'input-xxlarge',
                'div' => array('class' => 'input text input-before')
            ));
            echo $this->Form->input('Endereco.numero', array('label' => 'Número', 'class' => 'input-mini'));
            echo $this->Form->input('Endereco.bairro', array('div' => array('label' => 'Bairro*','class' => 'input text input-before')));
            echo $this->Form->input('Endereco.cidade', array('div' => array('label' => 'Cidade*','class' => 'input text input-before')));
            echo $this->Form->input('Endereco.estado', array('label' => 'Estado*'));
        ?>
    </fieldset>

    <fieldset>
        <legend>Dados de acesso</legend>
        <?php
            echo $this->Form->input('User.email',array('label' => 'E-mail*', 'class' => 'input-xxlarge'));
            echo $this->Form->input('User.password', array(
                'label' => 'Senha*', 
                'value' => '', 
                'class' => 'input-medium',
                'div' => array('class' => 'input text input-before')
            ));
            echo $this->Form->input('User.verify_password', array('label' => 'Repita a senha*', 'type' => 'password', 'value' => '', 'class' => 'input-medium'));
            
        ?>
    </fieldset>
    <div class="box-aviso">
        <h3>ATENÇÃO!</h3>
        <p>Ao clicar em <strong>Cadastrar</strong>, você declara ter preenchido corretamente os dados deste formulário</p>
    </div>
    <?php
    echo $this->Form->submit(
            'Cadastrar', array('class' => 'btn btn-large btn-success', 'title' => 'Clique para concluir seu cadastro')
    );
    ?>
</div>

<script>
    jQuery(function($){
       $("#CandidatoCpf").mask("999.999.999-99");
       $("#CandidatoDataNascimento").mask("99/99/9999");
       $("#EnderecoCep").mask("99999-999");
       $('#CandidatoTelefone1').mask("(99) 9999-9999?9");
       $('#CandidatoTelefone2').mask("(99) 9999-9999?9");
    });
    (function() {
        $('#EnderecoCep').blur(function() {
            cep = $(this).val();
            var url = "http://api.postmon.com.br/v1/cep/" + cep;
            $.getJSON(url)
            .done(function( data ) {
                $("#EnderecoLogradouro").val(unescape(data.logradouro));
                $("#EnderecoBairro").val(unescape(data.bairro));
                $("#EnderecoCidade").val(unescape(data.cidade));
                $("#EnderecoEstado").val(unescape(data.estado));
            })
            .fail(function() { 
                alert('CEP não encontrado. Informe o endeço manualmente')
                console.log( "error" ); 
            });
        });
      
    })();
</script>

<style type="text/css">
    .input-before {
        float: left;
        margin-right: 20px;
    }
</style>