<?php $this->extend('/Common/admin_edit'); ?>
<?php echo $this->Form->create('Etnia');?>
<fieldset>
	<div class="tabs">
		<ul>
			<li><a href="#etnia-main"><span><?php echo __('Etnia'); ?></span></a></li>
			<?php echo $this->Layout->adminTabs(); ?>
		</ul>

		<div id="etnia-main">
		<?php
			echo $this->Form->input('descricao', array ('label' => 'Descrição'));
		?>
		</div>
		<?php echo $this->Layout->adminTabs(); ?>
	</div>
</fieldset>

<div class="buttons">
<?php
	echo $this->Form->end(__('Salvar'));
	echo $this->Html->link(__('Cancelar'), array(
		'action' => 'index',
	), array(
		'class' => 'cancel',
	));
?>
</div>