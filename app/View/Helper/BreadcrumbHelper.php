<?php
/**
 * Breadcrumb Helper
 *
 * For custom theme specific methods.
 *
 * If your theme requires custom methods,
 * copy this file to /app/views/themed/your_theme_alias/helpers/custom.php and modify.
 *
 * You can then use this helper from your theme's views using $custom variable.
 *
 * @category Helper
 * @package  Croogo
 * @version  1.0
 * @author   Uziel Barbosa
 */
class BreadcrumbHelper extends AppHelper {
/**
 * Other helpers used by this helper
 *
 * @var array
 * @access public
 */
    
    public $helpers = array('Html');
    
    
    public $controllers = array(
        'acl_permissions' => 'Permissões',
        'acl_actions' => 'Ações',
        'etnias' => 'Etnias',
        'habilitacoes' => 'Habilitações',
        'nodes' => 'Conteúdo',
        'roles' => 'Perfils',
        'pages' => 'Opões',
        'users' => 'Usuários',
        'campus' => 'Campus',
        'cursos_nomes' => 'Nomes de cursos',
        'cursos' => 'Cursos',
        'exames' => 'Exames',
        'niveis_cursos' => 'Níveis de Cursos',
        'avisos' => 'Avisos',
        'anexos' => 'Anexos',
    );
    
    
    public $actions = array(
        'admin_index' => '',
        'admin_list' => '',
        'admin_add' => 'Adicionar',
        'admin_edit' => 'Editar',
        'admin_delete' => 'Excluir',
        'admin_save' => 'Salvar',
        'admin_dashboard' => 'Painel',
        'admin_reset_password' => 'Alterar senha',
        'admin_reset_password2' => 'Alterar senha',
        'admin_view' => 'Detalhes',
        'admin_prefix' => 'Configurações do site',
        'admin_imprimir' => 'Imprimir',
        'admin_finalizar' => 'Finalizar',
        'admin_mais_opcoes' => 'Mais opções',
        'admin_alterar_profissao' => 'Alterar profissão',
        'admin_filtrar_lista_imprimir' => 'Imprimir lista de funcionários',
        'admin_itens' => 'Itens',
        'admin_ofertas' => 'Ofertas de cursos',
        'admin_painel' => 'Painel',
    );
    
    
    
    function get() {
        $output = '';
        $controller = $this->params['controller'];
        
        $action = $this->params['action'];
        $output  .= $this->Html->link(__('Início', true), '/admin');
        $output  .= ' » ';
        $output  .= $this->Html->link(__($this->controllers[$controller], true), array('admin' => 'true','controller' => $controller, 'action' => 'index'));

        if ($this->actions[$action]) {
          $output  .= ' » ';
          $output  .= $this->Html->link(__($this->actions[$action], true), array('controller' => $controller, 'action' => $action));
        }
        
        return $output;
    }

}
?>