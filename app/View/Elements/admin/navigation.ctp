<!--<div id="nav">
<?php

foreach ($types_for_admin_layout as $t):
	CroogoNav::add('content.children.create.children.' . $t['Type']['alias'], array(
		'title' => $t['Type']['title'],
		'url' => array(
			'plugin' => false,
			'admin' => true,
			'controller' => 'nodes',
			'action' => 'add',
			$t['Type']['alias'],
			),
		));
endforeach;

foreach ($vocabularies_for_admin_layout as $v):
	$weight = 9999 + $v['Vocabulary']['weight'];
	CroogoNav::add('content.children.taxonomy.children.' . $v['Vocabulary']['alias'], array(
		'title' => $v['Vocabulary']['title'],
		'url' => array(
			'plugin' => false,
			'admin' => true,
			'controller' => 'terms',
			'action' => 'index',
			$v['Vocabulary']['id'],
			),
		'weight' => $weight,
		));
endforeach;

foreach ($menus_for_admin_layout as $m):
	$weight = 9999 + $m['Menu']['weight'];
	CroogoNav::add('menus.children.' . $m['Menu']['alias'], array(
		'title' => $m['Menu']['title'],
		'url' => array(
			'plugin' => false,
			'admin' => true,
			'controller' => 'links',
			'action' => 'index',
			$m['Menu']['id'],
			),
		'weight' => $weight,
		));
endforeach;

echo $this->Layout->adminMenus(CroogoNav::items());
?>
</div>-->


<div id="nav">
	<ul class="sf-menu">
		
            <li>
                <?php echo $this->Html->link(__('Painel', true), array('plugin' => null, 'controller' => 'pages', 'action' => 'dashboard')); ?>
            </li>
            
            <li>
                <?php echo $this->Html->link(__('Cursos', true), array('plugin' => null, 'controller' => 'cursos', 'action' => 'index')); ?>
            </li>

            <li>
                <?php echo $this->Html->link(__('Users', true), array('plugin' => null, 'controller' => 'users', 'action' => 'index')); ?>
                <ul>
                    <li><?php echo $this->Html->link(__('Users', true), array('plugin' => null, 'controller' => 'users', 'action' => 'index')); ?></li>
                    <li><?php echo $this->Html->link(__('Roles', true), array('plugin' => null, 'controller' => 'roles', 'action' => 'index')); ?></li>
                    <li><?php echo $this->Html->link(__('Permissions', true), array('plugin' => 'acl', 'controller' => 'acl_permissions', 'action' => 'index')); ?></li>
                </ul>
            </li>

            <li>
                <?php echo $this->Html->link(__('Settings', true), array('plugin' => null, 'controller' => 'settings', 'action' => 'prefix', 'Site')); ?>
                <ul>
                    <li><?php echo $this->Html->link(__('Site', true), array('plugin' => null, 'controller' => 'settings', 'action' => 'prefix', 'Site')); ?></li>
                    <li><?php echo $this->Html->link(__('Meta', true), array('plugin' => null, 'controller' => 'settings', 'action' => 'prefix', 'Meta')); ?></li>
                    <li><?php echo $this->Html->link(__('Etnias', true), array('plugin' => null, 'controller' => 'etnias', 'action' => 'index')); ?></li>
                    <li><?php echo $this->Html->link(__('Habilitações', true), array('plugin' => null, 'controller' => 'habilitacoes', 'action' => 'index')); ?></li>
                    <li><?php echo $this->Html->link(__('Campus', true), array('plugin' => null, 'controller' => 'campus', 'action' => 'index')); ?></li>
                    <li><?php echo $this->Html->link(__('Necessidades Especiais', true), array('plugin' => null, 'controller' => 'necessidades_especiais', 'action' => 'index')); ?></li>
                </ul>
            </li>
	</ul>
</div>