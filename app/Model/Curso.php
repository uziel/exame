<?php
/**
 * Curso
 *
 * PHP version 5
 *
 * @category Model
 * @package  Exame IFAL
 * @version  1.0
 * @author   Uziel Barbosa <contato@uzielbarbosa.com.br>
 * @license  http://www.opensource.org/licenses/mit-license.php The MIT License
 * @link     http://www.croogo.org
 */
class Curso extends AppModel {

/**
 * Model name
 *
 * @var string
 * @access public
 */
	public $name = 'Curso';



/**
 * Validation
 *
 * @var array
 * @access public
 */
	public $validate = array(
                'codigo' => array(
			'isUnique' => array(
				'rule' => 'isUnique',
				'message' => 'Curso já cadastrado com esse código',
			),
			'notEmpty' => array(
				'rule' => 'notEmpty',
				'message' => 'O campo deve ser informado.',
			),
		),
	);

/**
 * Display fields for this model
 *
 * @var array
 */
	protected $_displayFields = array(
		'id',
		'descricao',
                'codigo',
                'Campus.descricao' => 'Campus',
	);
        
        public $belongsTo = array(
                'CursoNome' => array(
			'className' => 'CursoNome',
			'foreignKey' => 'curso_nome_id',
			'conditions' => '',
			'fields' => '',
			'order' => '',
		),
                'Campus' => array(
			'className' => 'Campus',
			'foreignKey' => 'campus_id',
			'conditions' => '',
			'fields' => '',
			'order' => '',
		),
                'NivelCurso' => array(
			'className' => 'NivelCurso',
			'foreignKey' => 'nivel_id',
			'conditions' => '',
			'fields' => '',
			'order' => '',
		),
                'Turno' => array(
			'className' => 'Turno',
			'foreignKey' => 'turno_id',
			'conditions' => '',
			'fields' => '',
			'order' => '',
		),
	);

}
