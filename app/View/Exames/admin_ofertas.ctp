<?php $this->extend('/Common/admin_edit'); ?>
<?php echo $this->Form->create('ExameCurso');?>

    <?php
        echo $this->Form->input('exame_id', array ('type' => 'hidden', 'value' => $exameId));
    ?>
    <div class="input select">
        <label for="ExameCursoId">Curso*</label>
        <select id="ExameCursoId" name="data[ExameCurso][curso_id]" class="selectbox-big">
            <?php foreach ($cursos as $curso):?>
                <option value="<?php echo $curso['Curso']['id'];?>"><?php echo $curso['CursoDescricao']['descricao'];?></option>
            <?php endforeach;?>
        </select>
    </div>
    <?php
        echo $this->Form->input('numero_vagas', array ('label' => 'Número de vagas*', 'class' => 'input-big'));
    ?>
		
    <div class="clear"></div>
    
    <div class="buttons">
    <?php
            echo $this->Form->end(__('Salvar'));
            echo $this->Html->link(__('Cancelar'), array(
                    'action' => 'index',
            ), array(
                    'class' => 'cancel',
            ));
    ?>
    </div>
    
    <br/><br/><br/><br/>
    
    <h3>Cursos ofertados nesse exame:</h3>
    
    <table cellpadding="0" cellspacing="0">
            <?php
                    $tableHeaders =  $this->Html->tableHeaders(array(
                        'Código',
                        'Nome',
                        'Local',
                        'Turno',
                        'Total de vagas',
                        __('Ações', true),
                    ));
                    echo $tableHeaders;
                    
                    $rows = array();
                    foreach ($exameCursos AS $curso) {
                            $actions = $this->Html->link(__('Editar', true), array('controller' => 'exames', 'action' => 'edit_oferta', $curso['ExameCurso']['id']));
                            $actions .= ' ' . $this->Layout->adminRowActions($curso['ExameCurso']['id']);
                            $actions .= ' ' . $this->Html->link(__('Excluir', true), array(
                                    'controller' => 'exames',
                                    'action' => 'delete',
                                    $curso['ExameCurso']['id'],
                                    'token' => $this->params['_Token']['key'],
                            ), null, __('Are you sure?', true));

                            $rows[] = array(
                                    $curso['Curso']['codigo'],
                                    $curso['Curso']['CursoDescricao']['descricao'],
                                    $curso['Curso']['Campus']['descricao'],
                                    $curso['Curso']['Turno']['descricao'],
                                    $curso['ExameCurso']['numero_vagas'],
                                    $actions,
                            );
                    }

                    echo $this->Html->tableCells($rows);
            ?>
            </table>
    
    </div>
</div>


<style type="text/css">
    .input {
        height: 71px;
    }
    
    h3 {
        font-size: 19px;
    }
    
</style>