<!DOCTYPE html>
<html lang="en">
    <head>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title><?php echo $title_for_layout; ?> - <?php __('Croogo'); ?></title>
        <?php
        echo $scripts_for_layout;
        echo $this->fetch('meta');
        echo $this->fetch('css');
        echo $this->Html->css('/bootstrap/css/bootstrap');
        echo $this->Html->css('/bootstrap/css/bootstrap-responsive');
        echo $this->Html->css('login');
        ?>

    </head>
    <body>
        <div class="container">
            <div class="content">
                <div class="row logo">
                    <?php echo $this->Html->image('admin/logo.png'); ?>
                </div>
                <div class="row">
                   <?php
                        echo $content_for_layout;
                    ?>
                </div>
            </div>
        </div> <!-- /container -->
    </body>
</html>

