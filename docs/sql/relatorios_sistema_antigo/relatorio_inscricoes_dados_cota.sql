SELECT  i.id, i.numero_referencia, e.nome, i.reserva_vagas, e.nome, i.renda_familiar, i.total_dependentes FROM `inscricoes` i 
LEFT JOIN escolas e on (e.id = i.escola_id)
LEFT JOIN etnias et on (et.id = i.etnia_id)
LEFT JOIN cursos c on (c.id = i.curso_id)
LEFT JOIN exames ex on (ex.id = c.exame_id)
WHERE ex.id IN (41,42)
ORDER BY i.id ASC 