<?php
/**
 * Etnias Controller
 *
 * PHP version 5
 *
 * @category Controller
 * @package  Exame IFAL
 * @version  1.0
 * @author   Uziel Barbosa <contato@uzielbarbosa.com.br>
 * @license  http://www.opensource.org/licenses/mit-license.php The MIT License
 * @link     http://exame.ifal.edu.br
 */
class EtniasController extends AppController {

/**
 * Controller name
 *
 * @var string
 * @access public
 */
	public $name = 'Etnias';

/**
 * Models used by the Controller
 *
 * @var array
 * @access public
 */
	public $uses = array('Etnia');

	public function admin_index() {
		$this->set('title_for_layout', __('Etnias'));

		$this->Etnia->recursive = 0;
		$this->paginate['Etnia']['order'] = "Etnia.id ASC";
		$this->set('etnias', $this->paginate());
		$this->set('displayFields', $this->Etnia->displayFields());
	}

	public function admin_add() {
		$this->set('title_for_layout', __('Adicionar Etnia'));

		if (!empty($this->request->data)) {
			$this->Etnia->create();
			if ($this->Etnia->save($this->request->data)) {
				$this->Session->setFlash(__('Etnia cadastrada com sucesso'), 'default', array('class' => 'success'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Erro ao cadastrar etnia, tente novamente'), 'default', array('class' => 'error'));
			}
		}
	}

	public function admin_edit($id = null) {
		$this->set('title_for_layout', __('Editar Etnia'));

		if (!$id && empty($this->request->data)) {
			$this->Session->setFlash(__('Etnia inválida'), 'default', array('class' => 'error'));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->request->data)) {
			if ($this->Etnia->save($this->request->data)) {
				$this->Session->setFlash(__('Etnia alterada com sucesso'), 'default', array('class' => 'success'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Erro ao alterar etnia, tente novamente'), 'default', array('class' => 'error'));
			}
		}
		if (empty($this->request->data)) {
			$this->request->data = $this->Etnia->read(null, $id);
		}
	}

	public function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Id da Etnia inválido'), 'default', array('class' => 'error'));
			$this->redirect(array('action' => 'index'));
		}
		if ($this->Etnia->delete($id)) {
			$this->Session->setFlash(__('Etnia excluída com sucesso'), 'default', array('class' => 'success'));
			$this->redirect(array('action' => 'index'));
		}
	}

}
