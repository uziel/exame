--
-- Estrutura da tabela estados
--

CREATE TABLE IF NOT EXISTS estados (
  id int(11) NOT NULL AUTO_INCREMENT,
  descricao varchar(255) NOT NULL,
  uf varchar(2) NOT NULL,
  PRIMARY KEY (id)
);

--
-- Estrutura da tabela cidades
--

CREATE TABLE IF NOT EXISTS cidades (
  id int(11) NOT NULL AUTO_INCREMENT,
  estado_id int(11) NOT NULL,
  descricao varchar(255) NOT NULL,
  PRIMARY KEY (id),
  KEY estado_id (estado_id)
);


--
-- Estrutura da tabela etnias
--

CREATE TABLE IF NOT EXISTS etnias (
  id int(5) NOT NULL AUTO_INCREMENT,
  descricao varchar(30) NOT NULL,
  PRIMARY KEY (id)
);

--
-- Estrutura da tabela linguas
--

CREATE TABLE IF NOT EXISTS linguas (
  id int(5) NOT NULL AUTO_INCREMENT,
  descricao varchar(30) NOT NULL,
  PRIMARY KEY (id)
);

--
-- Estrutura da tabela necessidades_especiais
--

CREATE TABLE IF NOT EXISTS necessidades_especiais (
  id int(5) NOT NULL AUTO_INCREMENT,
  descricao varchar(30) NOT NULL,
  PRIMARY KEY (id)
);

--
-- Estrutura da tabela tipos_escola_origem
--

CREATE TABLE IF NOT EXISTS tipos_escola_origem (
  id int(5) NOT NULL AUTO_INCREMENT,
  descricao varchar(30) NOT NULL,
  PRIMARY KEY (id)
);

--
-- Estrutura da tabela habilitacoes
--

CREATE TABLE IF NOT EXISTS habilitacoes (
  id int(5) NOT NULL AUTO_INCREMENT,
  descricao varchar(30) NOT NULL,
  PRIMARY KEY (id)
);

--
-- Estrutura da tabela niveis_cursos
--

CREATE TABLE IF NOT EXISTS niveis_cursos (
  id int(5) NOT NULL AUTO_INCREMENT,
  descricao varchar(30) NOT NULL,
  codigo varchar(10) NOT NULL,
  PRIMARY KEY (id)
);


--
-- Estrutura da tabela eixos_cursos
--

CREATE TABLE IF NOT EXISTS eixos_cursos (
  id int(5) NOT NULL AUTO_INCREMENT,
  descricao varchar(30) NOT NULL,
  codigo varchar(10) NOT NULL,
  PRIMARY KEY (id)
);

--
-- Estrutura da tabela areas_cursos
--

CREATE TABLE IF NOT EXISTS areas_cursos (
  id int(5) NOT NULL AUTO_INCREMENT,
  descricao varchar(30) NOT NULL,
  codigo varchar(10) NOT NULL,
  PRIMARY KEY (id)
);

--
-- Estrutura da tabela modalidades_cursos
--

CREATE TABLE IF NOT EXISTS modalidades_cursos (
  id int(5) NOT NULL AUTO_INCREMENT,
  descricao varchar(30) NOT NULL,
  codigo varchar(10) NOT NULL,
  PRIMARY KEY (id)
);

--
-- Estrutura da tabela turnos
--

CREATE TABLE IF NOT EXISTS turnos (
  id int(5) NOT NULL AUTO_INCREMENT,
  descricao varchar(30) NOT NULL,
  codigo varchar(10) NOT NULL,
  PRIMARY KEY (id)
);

--
-- Estrutura da tabela tipos_anexos
--

CREATE TABLE IF NOT EXISTS tipos_anexos (
  id int(5) NOT NULL AUTO_INCREMENT,
  descricao varchar(30) NOT NULL,
  PRIMARY KEY (id)
);


--
-- Estrutura da tabela anexos
--

CREATE TABLE IF NOT EXISTS anexos (
  id int(11) NOT NULL AUTO_INCREMENT,
  exame_id int(11) NOT NULL,
  tipo_anexo_id int(11) NOT NULL,
  descricao varchar(255) NOT NULL,
  arquivo varchar(10) NOT NULL,
  PRIMARY KEY (id),
  KEY tipo_anexo_id (exame_id),
  KEY tipo_anexo_id (tipo_anexo_id)
);

--
-- Estrutura da tabela campus
--

CREATE TABLE IF NOT EXISTS campus (
  id int(5) NOT NULL AUTO_INCREMENT,
  descricao varchar(30) NOT NULL,
  PRIMARY KEY (id)
);

--
-- Estrutura da tabela cursos_descricoes 
--

CREATE TABLE IF NOT EXISTS cursos_descricoes (
  id int(5) NOT NULL AUTO_INCREMENT,
  nome varchar(100) NOT NULL,
  descricao text NOT NULL,
  PRIMARY KEY (id)
);

--
-- Estrutura da tabela cursos
--

CREATE TABLE IF NOT EXISTS cursos (
  id int(11) NOT NULL AUTO_INCREMENT,
  eixo_id int(11) NOT NULL,
  area_id int(11) NOT NULL,
  nivel_id int(11) NOT NULL,
  modalidade_id int(11) NOT NULL,
  turno_id int(11) NOT NULL,
  campus_id int(11) NOT NULL,
  user_id int(11) NOT NULL,    
  curso_descricao_id int(11) NOT NULL,    
  codigo varchar(50) NOT NULL,
  data_cadastro DATETIME NOT NULL, 
  PRIMARY KEY (id),
  KEY eixo_id (eixo_id),
  KEY area_id (area_id),
  KEY nivel_id (nivel_id),
  KEY modalidade_id (modalidade_id),
  KEY turno_id (turno_id),
  KEY campus_id (campus_id)
);

CREATE TABLE IF NOT EXISTS exames (
  id int(5) NOT NULL AUTO_INCREMENT,
  nome varchar(255) NOT NULL,
  descricao varchar(255) NOT NULL,
  data_inicio_inscricao_normal DATE NOT NULL,     
  data_fim_inscricao_normal DATE NOT NULL,     
  data_inicio_inscricao_isento DATE NOT NULL,     
  data_fim_inscricao_isento DATE NOT NULL,     
  data_exame DATE NOT NULL,     
  taxa_inscricao DECIMAL(10,2) NOT NULL,     
  PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS exames_cursos (
  id int(11) NOT NULL AUTO_INCREMENT,
  exame_id int(11) NOT NULL,
  curso_id int(11) NOT NULL,
  numero_vagas integer NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS avisos (
  id int(5) NOT NULL AUTO_INCREMENT,
  exame_id int(11) NOT NULL,
  titulo varchar(255) NOT NULL,
  texto text NOT NULL,
  PRIMARY KEY (id)
);