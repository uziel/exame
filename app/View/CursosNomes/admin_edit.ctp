<?php $this->extend('/Common/admin_edit'); ?>
<?php echo $this->Form->create('CursoNome');?>

    <?php
            echo $this->Form->input('id');
                echo $this->Form->input('tipo_curso_id', array (
                'label' => 'Tipo', 
                'class' => 'selectbox-big',
                'options' => $tiposCursos,
                'empty' => 'Selecione...'
            ));
            echo $this->Form->input('nome', array ('label' => 'Nome*', 'class' => 'input-big'));
            
    ?>
		
    <div class="clear"></div>

    <div class="buttons">
    <?php
            echo $this->Form->end(__('Salvar'));
            echo $this->Html->link(__('Cancelar'), array(
                    'action' => 'index',
            ), array(
                    'class' => 'cancel',
            ));
    ?>
    </div>
    </div>  
</div>    