<?php //$this->extend('/Common/admin_index'); ?>

<div class="exames index simplebox">
	 <div class="title">
            <?php echo $title_for_layout; ?>
            <span></span>
        </div>
        <div class="body">

            <div class="actions">
                    <ul>
                            <li><?php echo $this->Html->link(__('Novo Exame', true), array('action'=>'add')); ?></li>
                    </ul>
            </div>

            <table cellpadding="0" cellspacing="0">
            <?php
                    $tableHeaders =  $this->Html->tableHeaders(array(
                            $this->Paginator->sort('id','Código'),
                            $this->Paginator->sort('nome','Nome'),
                            $this->Paginator->sort('data_exame','Data do exame'),
                            $this->Paginator->sort('taxa_inscricao','Taxa de inscrição'),
                            __('Ações', true),
                    ));
                    echo $tableHeaders;

                    $rows = array();
                    foreach ($exames AS $exame) {
                            $actions  = $this->Html->link(__('Ofertas', true), array('controller' => 'exames', 'action' => 'ofertas', $exame['Exame']['id']));
                            $actions .= ' ' . $this->Html->link(__('Editar', true), array('controller' => 'exames', 'action' => 'edit', $exame['Exame']['id']));
                            $actions .= ' ' . $this->Layout->adminRowActions($exame['Exame']['id']);
                            $actions .= ' ' . $this->Html->link(__('Excluir', true), array(
                                    'controller' => 'exames',
                                    'action' => 'delete',
                                    $exame['Exame']['id'],
                                    'token' => $this->params['_Token']['key'],
                            ), null, __('Are you sure?', true));

                            $rows[] = array(
                                    $exame['Exame']['id'],
                                    $this->Html->link(h($exame['Exame']['nome']), array('controller' => 'exames', 'action' => 'painel', $exame['Exame']['id'])),
                                    date('d/m/Y', strtotime($exame['Exame']['data_exame'])),
                                    $exame['Exame']['taxa_inscricao'],
                                    $actions,
                            );
                    }

                    echo $this->Html->tableCells($rows);
            ?>
            </table>
        <div class="paging">
            <?php echo $this->Paginator->prev('');?>
            <?php echo $this->Paginator->numbers(array('separator' => '')); ?>
            <?php echo $this->Paginator->next('');?>
        </div>
    </div>
</div>
