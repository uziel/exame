<?php
#
?>
<ul class="nav">
    <li><a href="<?php echo $this->base;?>">Home</a></li>
    <li><a href="<?php echo $this->base;?>/cadastro">Cadastrar</a></li>
    <li><a href="">Esqueci minha senha</a></li>
    <li><a href="">Exames anteriores</a></li>
    <?php if($this->Session->read('Auth.User.id')):?>
        <li>
        <?php echo $this->Html->link('Sair', array ('controller' => 'users', 'action' => 'logout'));?>
        </li>
    <?php endif; ?>
</ul>

<?php if(!$this->Session->read('Auth.User.id')):?>
    <?php echo $this->Form->create('User', array('class' => 'navbar-form pull-right','url' => array('controller' => 'users', 'action' => 'login'))); ?>
        <?php
        echo $this->Form->input('username', array('label' => false,'placeholder' => 'CPF', 'class' => 'span2','div' => false));
        echo $this->Form->input('password', array('label' => false,'placeholder' => 'Senha', 'class' => 'span2','div' => false));
        ?>
    <?php
    ?>
    <button  type="submit" class="btn btn-success">Entrar</button>
    </form>
<?php endif; ?>