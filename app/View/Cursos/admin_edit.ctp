<?php $this->extend('/Common/admin_edit'); ?>
<?php echo $this->Form->create('Curso');?>

    <?php
            echo $this->Form->input('id');
            echo $this->Form->input('curso_nome_id', array ('label' => 'Curso', 'options' => $cursos,'class' => 'selectbox-big'));
        echo $this->Form->input('nivel_id', array ('label' => 'Nível*', 'options' => $niveis, 'class' => 'selectbox-big'));
        echo $this->Form->input('campus_id', array ('label' => 'Campus*', 'options' => $campus, 'class' => 'selectbox-big'));
        echo $this->Form->input('turno_id', array ('label' => 'Turno*', 'options' => $turnos, 'class' => 'selectbox-big'));
        echo $this->Form->input('codigo', array ('label' => 'Código', 'disabled' => true));
    ?>
		
    <div class="clear"></div>

    <div class="buttons">
    <?php
            echo $this->Form->end(__('Salvar'));
            echo $this->Html->link(__('Cancelar'), array(
                    'action' => 'index',
            ), array(
                    'class' => 'cancel',
            ));
    ?>
    </div>
    </div>  
</div>    