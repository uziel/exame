<?php
/**
 * Campus
 *
 * PHP version 5
 *
 * @category Model
 * @package  Exame IFAL
 * @version  1.0
 * @author   Uziel Barbosa <contato@uzielbarbosa.com.br>
 * @license  http://www.opensource.org/licenses/mit-license.php The MIT License
 * @link     http://www.croogo.org
 */
class Campus extends AppModel {

/**
 * Model name
 *
 * @var string
 * @access public
 */
	public $name = 'Campus';
        
        public $useTable = 'campus';



/**
 * Validation
 *
 * @var array
 * @access public
 */
	public $validate = array(
                'descricao' => array(
			'isUnique' => array(
				'rule' => 'isUnique',
				'message' => 'Campus já cadastrada',
			),
			'notEmpty' => array(
				'rule' => 'notEmpty',
				'message' => 'O campo deve ser informado.',
			),
		),
                'codigo' => array(
			'isUnique' => array(
				'rule' => 'isUnique',
				'message' => 'Campus já cadastrado com esse código',
			),
			'notEmpty' => array(
				'rule' => 'notEmpty',
				'message' => 'O campo deve ser informado.',
			),
		),
	);

/**
 * Display fields for this model
 *
 * @var array
 */
	protected $_displayFields = array(
		'id',
		'descricao',
	);

}
