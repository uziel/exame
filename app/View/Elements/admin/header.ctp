<?php ;?>

<div class="logo">
    <a href="<?php echo $this->base?>/admin">
        <?php echo $this->Html->image('admin/logo.png');?>
    </a>
</div>

<div class="welcome">
    <?php echo $this->Html->image('admin/avatar.png', array('class' => 'avatar'));?>
    <b>Bem-vindo(a)</b>	<?php echo $this->Session->read('Auth.User.username');?>
    <a title="Sair" class="logout" href="<?php echo $this->base;?>/admin/users/logout">Sair</a>
    <div class="clear"></div>
    <div id='alterar-senha'>
        <a href="<?php echo $this->base;?>/admin/users/reset_password2">Alterar senha</a>
    </div>
</div>

<div class="clear"></div>