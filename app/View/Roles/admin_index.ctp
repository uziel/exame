<div class="roles simplebox">
    
    <div class="title">
        <?php echo $title_for_layout; ?>
        <span></span>
    </div>
    <div class="body">
        <div class="actions">
            <ul>
                <li><?php echo $this->Html->link(__('New Role', true), array('action'=>'add')); ?></li>
            </ul>
        </div>

        <table cellpadding="0" cellspacing="0">
        <?php
            $tableHeaders =  $this->Html->tableHeaders(array(
                $this->Paginator->sort('id'),
                $this->Paginator->sort('title'),
                $this->Paginator->sort('alias'),
                __('Actions', true),
            ));
            echo $tableHeaders;

            $rows = array();
            foreach ($roles AS $role) {
                $actions  = $this->Html->link(__('Edit', true), array('controller' => 'roles', 'action' => 'edit', $role['Role']['id']));
                $actions .= ' ' . $this->Layout->adminRowActions($role['Role']['id']);
                $actions .= ' ' . $this->Html->link(__('Delete', true), array(
                    'controller' => 'roles',
                    'action' => 'delete',
                    $role['Role']['id'],
                    'token' => $this->params['_Token']['key'],
                ), null, __('Are you sure?', true));

                $rows[] = array(
                    $role['Role']['id'],
                    $role['Role']['title'],
                    $role['Role']['alias'],
                    $actions,
                );
            }

            echo $this->Html->tableCells($rows);
        ?>
        </table>
        <div class="paging">
            <?php echo $this->Paginator->prev('');?>
            <?php echo $this->Paginator->numbers(array('separator' => '')); ?>
            <?php echo $this->Paginator->next('');?>
        </div>
    </div>
</div>