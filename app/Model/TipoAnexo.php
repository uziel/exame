<?php
/**
 * TipoAnexo
 *
 * PHP version 5
 *
 * @category Model
 * @package  Exame IFAL
 * @version  1.0
 * @author   Uziel Barbosa <contato@uzielbarbosa.com.br>
 * @license  http://www.opensource.org/licenses/mit-license.php The MIT License
 * @link     http://www.croogo.org
 */
class TipoAnexo extends AppModel {

/**
 * Model name
 *
 * @var string
 * @access public
 */
	public $name = 'TipoAnexo';
        
        public $useTable = 'tipos_anexos';



/**
 * Validation
 *
 * @var array
 * @access public
 */
	public $validate = array(
                'descricao' => array(
			'isUnique' => array(
				'rule' => 'isUnique',
				'message' => 'TipoAnexo já cadastrada',
			),
			'notEmpty' => array(
				'rule' => 'notEmpty',
				'message' => 'O campo deve ser informado.',
			),
		),
	);

/**
 * Display fields for this model
 *
 * @var array
 */
	protected $_displayFields = array(
		'id',
		'descricao',
	);

}
