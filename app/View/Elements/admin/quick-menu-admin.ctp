<?php ;?>

<div class="in">
    
    <a class="button" href="<?php echo $this->base;?>/admin/exames">
        <?php echo $this->Html->image('admin/quick-icon/exames.png');?>
        <span>Exames</span>
    </a>
    
    <a class="button" href="<?php echo $this->base;?>/admin/cursos">
        <?php echo $this->Html->image('admin/quick-icon/cursos.png');?>
        <span>Cursos</span>
    </a>
    
    <a class="button" href="<?php echo $this->base;?>/admin/campus">
        <?php echo $this->Html->image('admin/quick-icon/campus.png');?>
        <span>Campus</span>
    </a>
    
    <a class="button" href="<?php echo $this->base;?>/admin/escolas">
        <?php echo $this->Html->image('admin/quick-icon/escolas.png');?>
        <span>Escolas</span>
    </a>
    
    <a class="button" href="<?php echo $this->base;?>/admin/users">
        <?php echo $this->Html->image('admin/quick-icon/usuarios.png');?>
        <span>Usuários</span>	
    </a>
    
    <a class="button" href="<?php echo $this->base;?>/admin/roles">
        <?php echo $this->Html->image('admin/quick-icon/perfis.png');?>
        <span>Perfis</span>
    </a>
    
    <a class="button" href="<?php echo $this->base;?>/admin/acl/acl_permissions">
        <?php echo $this->Html->image('admin/quick-icon/permissoes.png');?>
        <span>Permissões</span>
    </a>

    <a class="button" href="<?php echo $this->base;?>/admin/pages/mais_opcoes">
        <?php echo $this->Html->image('admin/quick-icon/config.png');?>
        <span>Mais opções</span>
    </a>
    
<div class="clear"></div>
</div>