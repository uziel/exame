<?php
/**
 * Necessidades Especiais Controller
 *
 * PHP version 5
 *
 * @category Controller
 * @package  Exame IFAL
 * @version  1.0
 * @author   Uziel Barbosa <contato@uzielbarbosa.com.br>
 * @license  http://www.opensource.org/licenses/mit-license.php The MIT License
 * @link     http://exame.ifal.edu.br
 */
class NecessidadesEspeciaisController extends AppController {

/**
 * Controller name
 *
 * @var string
 * @access public
 */
	public $name = 'NecessidadesEspeciais';

/**
 * Models used by the Controller
 *
 * @var array
 * @access public
 */
	public $uses = array('NecessidadeEspecial');

	public function admin_index() {
		$this->set('title_for_layout', __('Necessidades Especiais'));

		$this->NecessidadeEspecial->recursive = 0;
		$this->paginate['NecessidadeEspecial']['order'] = "NecessidadeEspecial.id ASC";
		$this->set('necessidades', $this->paginate());
		$this->set('displayFields', $this->NecessidadeEspecial->displayFields());
	}

	public function admin_add() {
		$this->set('title_for_layout', __('Adicionar Necessidade Especial'));

		if (!empty($this->request->data)) {
			$this->NecessidadeEspecial->create();
			if ($this->NecessidadeEspecial->save($this->request->data)) {
				$this->Session->setFlash(__('Necessidade especial cadastrada com sucesso'), 'default', array('class' => 'success'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Erro ao cadastrar necessidade especial, tente novamente'), 'default', array('class' => 'error'));
			}
		}
	}

	public function admin_edit($id = null) {
		$this->set('title_for_layout', __('Editar Necessidade Especial'));

		if (!$id && empty($this->request->data)) {
			$this->Session->setFlash(__('Necessidade de especial inválida'), 'default', array('class' => 'error'));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->request->data)) {
			if ($this->NecessidadeEspecial->save($this->request->data)) {
				$this->Session->setFlash(__('Necessidade especial alterada com sucesso'), 'default', array('class' => 'success'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Erro ao alterar necessidade especial, tente novamente'), 'default', array('class' => 'error'));
			}
		}
		if (empty($this->request->data)) {
			$this->request->data = $this->NecessidadeEspecial->read(null, $id);
		}
	}

	public function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Id da Necessidade Especial inválido'), 'default', array('class' => 'error'));
			$this->redirect(array('action' => 'index'));
		}
		if ($this->NecessidadeEspecial->delete($id)) {
			$this->Session->setFlash(__('Necessidade especial excluída com sucesso'), 'default', array('class' => 'success'));
			$this->redirect(array('action' => 'index'));
		}
	}

}
