<?php

App::uses('AuthComponent', 'Controller/Component');

/**
 * User
 *
 * PHP version 5
 *
 * @category Model
 * @package  Croogo
 * @version  1.0
 * @author   Fahad Ibnay Heylaal <contact@fahad19.com>
 * @license  http://www.opensource.org/licenses/mit-license.php The MIT License
 * @link     http://www.croogo.org
 */
class User extends AppModel {

/**
 * Model name
 *
 * @var string
 * @access public
 */
	public $name = 'User';

/**
 * Order
 *
 * @var string
 * @access public
 */
	public $order = 'name ASC';

/**
 * Behaviors used by the Model
 *
 * @var array
 * @access public
 */
	public $actsAs = array(
		'Acl' => array(
			'className' => 'CroogoAcl',
			'type' => 'requester',
			),
	);

/**
 * Model associations: belongsTo
 *
 * @var array
 * @access public
 */
	public $belongsTo = array('Role');

/**
 * Validation
 *
 * @var array
 * @access public
 */
	public $validate = array(
		'username' => array(
			'isUnique' => array(
				'rule' => 'isUnique',
				'message' => 'O CPF informado já é de uma pessoa cadastrada no sistema',
			),
			'notEmpty' => array(
				'rule' => 'notEmpty',
				'message' => 'O campo deve ser informado',
			),
		),
		'email' => array(
			'email' => array(
				'rule' => 'email',
				'message' => 'Informe um e-mail válido',
			),
			'isUnique' => array(
				'rule' => 'isUnique',
				'message' => 'O e-mail informado já é de uma pessoa cadastrada no sistema',
			),
		),
		'password' => array(
			'rule' => array('minLength', 5),
			'message' => 'A senha deve possuir no mínimo 5 digitos',
		),
		'verify_password' => array(
			'rule' => '_identical',
			),
		'name' => array(
			'rule' => 'notEmpty',
			'message' => 'O campo deve ser informado',
		),
	);

/**
 * Display fields for this model
 *
 * @var array
 */
	protected $_displayFields = array(
		'id',
		'Role.title' => 'Role',
		'username',
		'name',
		'status' => array('type' => 'boolean'),
		'email',
	);

/**
 * Edit fields for this model
 *
 * @var array
 */
	protected $_editFields = array(
		'role_id',
		'username',
		'name',
		'email',
		'website',
		'status',
	);

	public function beforeDelete($cascade = true) {
		$this->Role->Behaviors->attach('Aliasable');
		$adminRoleId = $this->Role->byAlias('admin');
		if ($this->field('role_id') == $adminRoleId) {
			$count = $this->find('count', array(
				'conditions' => array(
					'User.id <>' => $this->id,
					'User.role_id' => $adminRoleId,
					'User.status' => true,
					)
				));
			if ($count >= 1) {
				return true;
			}
		}
		return false;
	}

	public function beforeSave($options = array()) {
		if (!empty($this->data['User']['password'])) {
			$this->data['User']['password'] = AuthComponent::password($this->data['User']['password']);
		}
		return true;
	}

	protected function _identical($check) {
		if (isset($this->data['User']['password'])) {
			if ($this->data['User']['password'] != $check['verify_password']) {
				return __('As senhas não conferem');
			}
		}
		return true;
	}

}
