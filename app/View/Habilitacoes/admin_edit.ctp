<?php $this->extend('/Common/admin_edit'); ?>
<?php echo $this->Form->create('Habilitacao');?>
<fieldset>
	<div class="tabs">
		<ul>
			<li><a href="#habilitacao-main"><span><?php echo __('Habilitação'); ?></span></a></li>
			<?php echo $this->Layout->adminTabs(); ?>
		</ul>

		<div id="habilitacao-main">
		<?php
			echo $this->Form->input('id');
			echo $this->Form->input('descricao', array ('label' => 'Descrição'));
		?>
		</div>
		<?php echo $this->Layout->adminTabs(); ?>
	</div>
</fieldset>

<div class="buttons">
<?php
	echo $this->Form->end(__('Salvar'));
	echo $this->Html->link(__('Cancelar'), array(
		'action' => 'index',
	), array(
		'class' => 'cancel',
	));
?>
</div>