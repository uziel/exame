<?php $this->Html->script(array('jquery/jquery.maskedinput'), false); ?>

<?php $this->extend('/Common/admin_edit'); ?>
<?php echo $this->Form->create('Exame');?>

    <?php
            echo $this->Form->input('tipo_exame_id', array (
                'label' => 'Tipo', 
                'class' => 'input-big',
                'options' => $tiposExames
            ));
            echo $this->Form->input('nome', array ('label' => 'Nome', 'class' => 'input-big'));
            echo $this->Form->input('taxa_inscricao', array ('label' => 'Taxa de inscrição*', 'class' => 'input-big'));
            echo $this->Form->input('data_inicio_inscricao_normal', array ('label' => 'Data de início da inscrição normal*', 'class' => 'input-big', 'type' => 'text'));
            echo $this->Form->input('data_fim_inscricao_normal', array ('label' => 'Data final da inscrição normal*', 'class' => 'input-big', 'type' => 'text'));
            echo $this->Form->input('possui_isencao', array (
                'label' => 'O exame possui perido de isencao?', 
                'type' => 'radio',
                'default' => '0',
                'options' => array('0' => 'Nao', '1' => 'Sim')
            ));
            echo $this->Form->input('data_inicio_inscricao_isento', array ('label' => 'Data de início da inscrição isento*', 'class' => 'input-big', 'type' => 'text'));
            echo $this->Form->input('data_fim_inscricao_isento', array ('label' => 'Data final da inscrição isento*', 'class' => 'input-big', 'type' => 'text'));
            echo $this->Form->input('data_exame', array ('label' => 'Data do exame*', 'class' => 'input-big', 'type' => 'text'));
            echo $this->Form->input('taxa_inscricao', array ('label' => 'Valor*', 'class' => 'input-big', 'type' => 'text'));
            echo $this->Form->input('lingua_estrangeira', array (
                'label' => 'Lingua estrangeira?', 
                'type' => 'radio',
                'default' => '0',
                'options' => array('0' => 'Nao', '1' => 'Sim')
            ));
    ?>
		
    <div class="clear"></div>
    
    <div class="buttons">
    <?php
            echo $this->Form->end(__('Salvar'));
            echo $this->Html->link(__('Cancelar'), array(
                    'action' => 'index',
            ), array(
                    'class' => 'cancel',
            ));
    ?>
    </div>
    </div>
</div>


<style type="text/css">
    .input, .select {
        min-height: 80px;
    }
    
    .radio, .checkbox {
        width: 400px;
        margin-right: 30px;
    }
    
    .radio label {
        float: left;
        margin-top: 0;
        padding: 2px 0 0 0;
    }
    .radio input {
        float: left;
    }
</style>


<script type="text/javascript">
    
    jQuery(function($){
       $("#EnderecoCep").mask("99999-999");
       
       $("#ExameDataInicioInscricaoNormal").mask("99/99/9999").datepicker({dateFormat: "dd/mm/yy"});
       $("#ExameDataFimInscricaoNormal").mask("99/99/9999").datepicker({dateFormat: "dd/mm/yy"});
       $("#ExameDataInicioInscricaoIsento").mask("99/99/9999").datepicker({dateFormat: "dd/mm/yy"});
       $("#ExameDataFimInscricaoIsento").mask("99/99/9999").datepicker({dateFormat: "dd/mm/yy"});
       $("#ExameDataExame").mask("99/99/9999").datepicker({dateFormat: "dd/mm/yy"});
       
    });
    
</script>