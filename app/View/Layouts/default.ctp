<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo $this->Html->charset(); ?>
        <title>
            Exame IFAL - 
            <?php echo $title_for_layout; ?>
        </title>

        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- Le styles -->
        <style type="text/css">
            body {
                padding-top: 60px;
                padding-bottom: 40px;
            }
        </style>
        
        <?php
            echo $this->Html->css(array(
                '/bootstrap/css/bootstrap',
                '/bootstrap/css/bootstrap-responsive',
                'style',
            ));
            echo $this->Layout->js();
            echo $this->Html->script(array(
                'jquery/jquery.min',
                '/bootstrap/js/bootstrap.min',
            ));
            echo $scripts_for_layout;
        ?>

        <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
          <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->

    </head>

    <body>
        
        <div class="navbar navbar-inverse navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container">
                    <a class="brand" href="#">Exame de seleção</a>
                    <div class="nav-collapse collapse">
                        <?php echo $this->element('top_menu'); ?>
                    </div><!--/.nav-collapse -->
                </div>
            </div>
        </div>

        <div class="container">
            
            <div class="mensagens">
                <?php
                    echo $this->Layout->sessionFlash();
                ?>
            </div>

            <?php echo $content_for_layout;?>

            <hr>

            <footer>
                <p>Exame de seleção / IFAL <?php echo date('Y');?></p>
            </footer>

        </div> <!-- /container -->

    </body>
</html>
