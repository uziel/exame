<?php //$this->extend('/Common/admin_index'); ?>

<div class="cursos index simplebox">
	 <div class="title">
            <?php echo $title_for_layout; ?>
            <span></span>
        </div>
        <div class="body">

            <div class="actions">
                    <ul>
                            <li><?php echo $this->Html->link(__('Novo Curso', true), array('action'=>'add')); ?></li>
                    </ul>
            </div>

            <table cellpadding="0" cellspacing="0">
            <?php
                    $tableHeaders =  $this->Html->tableHeaders(array(
                            $this->Paginator->sort('codigo','Código'),
                            $this->Paginator->sort('Curso.curso_descricao_id','Curso'),
                            $this->Paginator->sort('Curso.campus_id','Campus'),
                            __('Ações', true),
                    ));
                    echo $tableHeaders;

                    $rows = array();
                    foreach ($cursos AS $curso) {
                            $actions  = $this->Html->link(__('Editar', true), array('controller' => 'cursos', 'action' => 'edit', $curso['Curso']['id']));
                            $actions .= ' ' . $this->Layout->adminRowActions($curso['Curso']['id']);
                            $actions .= ' ' . $this->Html->link(__('Excluir', true), array(
                                    'controller' => 'cursos',
                                    'action' => 'delete',
                                    $curso['Curso']['id'],
                                    'token' => $this->params['_Token']['key'],
                            ), null, __('Are you sure?', true));

                            $rows[] = array(
                                    h($curso['Curso']['codigo']),
                                    h($curso['CursoNome']['nome']),
                                    h($curso['Campus']['descricao']),
                                    $actions,
                            );
                    }

                    echo $this->Html->tableCells($rows);
            ?>
            </table>
        <div class="paging">
            <?php echo $this->Paginator->prev('');?>
            <?php echo $this->Paginator->numbers(array('separator' => '')); ?>
            <?php echo $this->Paginator->next('');?>
        </div>
    </div>
</div>
