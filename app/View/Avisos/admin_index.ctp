<?php //$this->extend('/Common/admin_index'); ?>

<div class="avisos index simplebox">
	 <div class="title">
            <?php echo $title_for_layout; ?>
            <span></span>
        </div>
        <div class="body">

            <div class="actions">
                    <ul>
                            <li><?php echo $this->Html->link(__('Novo Aviso', true), array('action'=>'add', $exame['Exame']['id'])); ?></li>
                    </ul>
            </div>

            <table cellpadding="0" cellspacing="0">
            <?php
                    $tableHeaders =  $this->Html->tableHeaders(array(
                            $this->Paginator->sort('id','Código'),
                            $this->Paginator->sort('nome','Nome'),
                            $this->Paginator->sort('data_exame','Data do aviso'),
                            $this->Paginator->sort('taxa_inscricao','Taxa de inscrição'),
                            __('Ações', true),
                    ));
                    echo $tableHeaders;

                    $rows = array();
                    foreach ($avisos AS $aviso) {
                            $actions  = $this->Html->link(__('Ofertas', true), array('controller' => 'avisos', 'action' => 'ofertas', $aviso['Aviso']['id']));
                            $actions .= ' ' . $this->Html->link(__('Editar', true), array('controller' => 'avisos', 'action' => 'edit', $aviso['Aviso']['id']));
                            $actions .= ' ' . $this->Layout->adminRowActions($aviso['Aviso']['id']);
                            $actions .= ' ' . $this->Html->link(__('Excluir', true), array(
                                    'controller' => 'avisos',
                                    'action' => 'delete',
                                    $aviso['Aviso']['id'],
                                    'token' => $this->params['_Token']['key'],
                            ), null, __('Are you sure?', true));

                            $rows[] = array(
                                    $aviso['Aviso']['id'],
                                    $this->Html->link(h($aviso['Aviso']['nome']), array('controller' => 'avisos', 'action' => 'painel', $aviso['Aviso']['id'])),
                                    date('d/m/Y', strtotime($aviso['Aviso']['data_exame'])),
                                    $aviso['Aviso']['taxa_inscricao'],
                                    $actions,
                            );
                    }

                    echo $this->Html->tableCells($rows);
            ?>
            </table>
        <div class="paging">
            <?php echo $this->Paginator->prev('');?>
            <?php echo $this->Paginator->numbers(array('separator' => '')); ?>
            <?php echo $this->Paginator->next('');?>
        </div>
    </div>
</div>
