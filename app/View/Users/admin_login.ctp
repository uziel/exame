

<div class="users login-form">
    
    <h2><?php echo $title_for_layout; ?></h2>
    
    <?php echo $this->Layout->sessionFlash(); ?>
    <?php echo $this->Form->create('User', array('url' => array('controller' => 'users', 'action' => 'login'))); ?>
    <fieldset>
        <?php
        echo $this->Form->input('username', array('label' => false,'placeholder' => 'Usuário'));
        echo $this->Form->input('password', array('label' => false,'placeholder' => 'Senha'));
        ?>
    </fieldset>
    <?php
    echo $this->Form->submit(
        'Acessar', 
        array('class' => 'btn btn-success', 'title' => 'Custom Title')
    );
    ?>
</div>