<?php
/**
 * Pages Controller
 *
 * PHP version 5
 *
 * @category Controller
 * @package  Croogo
 * @version  1.0
 * @author   Fahad Ibnay Heylaal <contact@fahad19.com>
 * @license  http://www.opensource.org/licenses/mit-license.php The MIT License
 * @link     http://www.croogo.org
 */
class PagesController extends AppController {

/**
 * Controller name
 *
 * @var string
 * @access public
 */
	public $name = 'Pages';
        
/**
 * Models used by the Controller
 *
 * @var array
 * @access public
 */
	public $uses = array();

/**
 * Helpers used by the Controller
 *
 * @var array
 * @access public
 */
	public $helpers = array('Html', 'Form');

	public function admin_dashboard() {
		$this->set('title_for_layout', __('Dashboard'));
	}
        
        public function admin_mais_opcoes() {
		$this->set('title_for_layout', __('Mais opções'));
	}

	public function index() {
		$this->set('title_for_layout', __('Início'));
	}

}
