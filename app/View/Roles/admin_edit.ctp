<div class="roles simplebox">
    
    <div class="title">
        <?php echo $title_for_layout; ?>
        <span></span>
    </div>
    
    <div class="body">
        <?php echo $this->Form->create('Role');?>
        <?php
            echo $this->Form->input('id');
            echo $this->Form->input('title', array('class' => 'input-big'));
            echo $this->Form->input('alias', array('class' => 'input-big'));
        ?>
        <div class="clear"></div>
        <div class="buttons">
        <?php
            echo $this->Form->end(__('Salvar', true));
            echo $this->Html->link(__('Cancelar', true), array(
                'action' => 'index',
            ), array(
                'class' => 'cancel',
            ));
        ?>
        </div>
    </div>
</div>