<?php
/**
 * Curso
 *
 * PHP version 5
 *
 * @category Model
 * @package  Exame IFAL
 * @version  1.0
 * @author   Uziel Barbosa <contato@uzielbarbosa.com.br>
 * @license  http://www.opensource.org/licenses/mit-license.php The MIT License
 * @link     http://www.croogo.org
 */
class Candidato extends AppModel {

/**
 * Model name
 *
 * @var string
 * @access public
 */
	public $name = 'Candidato';



/**
 * Validation
 *
 * @var array
 * @access public
 */
	public $validate = array(
            'nome' => array(
                'notEmpty' => array(
                        'rule' => 'notEmpty',
                        'message' => 'O campo deve ser informado.',
                ),
            ),
            'nome_mae' => array(
                'notEmpty' => array(
                        'rule' => 'notEmpty',
                        'message' => 'O campo deve ser informado.',
                ),
            ),
            'cpf' => array(
                'isUnique' => array(
                        'rule' => 'isUnique',
                        'message' => 'O CPF informado já é de uma pessoa cadastrada no sistema',
                ),
                'notEmpty' => array(
                        'rule' => 'notEmpty',
                        'message' => 'O campo deve ser informado',
                ),
            ),
            'data_nascimento' => array(
                'rule' => 'date',
                'message' => 'O campo deve conter uma data válida.',
                'allowEmpty' => false
            ),
            'telefone_1' => array(
                'notEmpty' => array(
                        'rule' => 'notEmpty',
                        'message' => 'O campo deve ser informado.',
                ),
            ),
            'etnia_id' => array(
                'notEmpty' => array(
                        'rule' => 'notEmpty',
                        'message' => 'O campo deve ser informado.',
                ),
            ),
	);

/**
 * Display fields for this model
 *
 * @var array
 */
	protected $_displayFields = array(
		'id',
		'nome',
                'nome_mae',
	);
        
        public $belongsTo = array(
                'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => '',
		),
                'Endereco' => array(
			'className' => 'Endereco',
			'foreignKey' => 'endereco_id',
			'conditions' => '',
			'fields' => '',
			'order' => '',
		),
                'Etnia' => array(
			'className' => 'Etnia',
			'foreignKey' => 'etnia_id',
			'conditions' => '',
			'fields' => '',
			'order' => '',
		),
	);
        
}
