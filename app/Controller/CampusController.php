<?php
/**
 * Campus Controller
 *
 * PHP version 5
 *
 * @category Controller
 * @package  Exame IFAL
 * @version  1.0
 * @author   Uziel Barbosa <contato@uzielbarbosa.com.br>
 * @license  http://www.opensource.org/licenses/mit-license.php The MIT License
 * @link     http://exame.ifal.edu.br
 */
class CampusController extends AppController {

/**
 * Controller name
 *
 * @var string
 * @access public
 */
	public $name = 'Campus';

/**
 * Models used by the Controller
 *
 * @var array
 * @access public
 */
	public $uses = array('Campus');

	public function admin_index() {
		$this->set('title_for_layout', __('Campus'));

		$this->Campus->recursive = 0;
		$this->paginate['Campus']['order'] = "Campus.codigo ASC";
		$this->set('campus', $this->paginate());
		$this->set('displayFields', $this->Campus->displayFields());
	}

	public function admin_add() {
		$this->set('title_for_layout', __('Adicionar Campus'));

		if (!empty($this->request->data)) {
			$this->Campus->create();
			if ($this->Campus->save($this->request->data)) {
				$this->Session->setFlash(__('Campus cadastrado com sucesso'), 'default', array('class' => 'success'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Erro ao cadastrar campus, tente novamente'), 'default', array('class' => 'error'));
			}
		}
	}

	public function admin_edit($id = null) {
		$this->set('title_for_layout', __('Editar Campus'));

		if (!$id && empty($this->request->data)) {
			$this->Session->setFlash(__('Campus inválida'), 'default', array('class' => 'error'));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->request->data)) {
			if ($this->Campus->save($this->request->data)) {
				$this->Session->setFlash(__('Campus alterado com sucesso'), 'default', array('class' => 'success'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Erro ao alterar campus, tente novamente'), 'default', array('class' => 'error'));
			}
		}
		if (empty($this->request->data)) {
			$this->request->data = $this->Campus->read(null, $id);
		}
	}

	public function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Id do Campus inválido'), 'default', array('class' => 'error'));
			$this->redirect(array('action' => 'index'));
		}
		if ($this->Campus->delete($id)) {
			$this->Session->setFlash(__('Campus excluído com sucesso'), 'default', array('class' => 'success'));
			$this->redirect(array('action' => 'index'));
		}
	}

}
