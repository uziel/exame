-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tempo de Geração: 09/09/2012 às 04h37min
-- Versão do Servidor: 5.5.16
-- Versão do PHP: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Banco de Dados: `exame`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `acos`
--

CREATE TABLE IF NOT EXISTS `acos` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) DEFAULT NULL,
  `model` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `foreign_key` int(10) DEFAULT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lft` int(10) DEFAULT NULL,
  `rght` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=65 ;

--
-- Extraindo dados da tabela `acos`
--

INSERT INTO `acos` (`id`, `parent_id`, `model`, `foreign_key`, `alias`, `lft`, `rght`) VALUES
(1, NULL, NULL, NULL, 'controllers', 1, 128),
(2, 1, NULL, NULL, 'Pages', 2, 7),
(3, 2, NULL, NULL, 'admin_dashboard', 3, 4),
(4, 2, NULL, NULL, 'index', 5, 6),
(5, 1, NULL, NULL, 'Roles', 8, 17),
(6, 5, NULL, NULL, 'admin_index', 9, 10),
(7, 5, NULL, NULL, 'admin_add', 11, 12),
(8, 5, NULL, NULL, 'admin_edit', 13, 14),
(9, 5, NULL, NULL, 'admin_delete', 15, 16),
(10, 1, NULL, NULL, 'Settings', 18, 37),
(11, 10, NULL, NULL, 'admin_dashboard', 19, 20),
(12, 10, NULL, NULL, 'admin_index', 21, 22),
(13, 10, NULL, NULL, 'admin_view', 23, 24),
(14, 10, NULL, NULL, 'admin_add', 25, 26),
(15, 10, NULL, NULL, 'admin_edit', 27, 28),
(16, 10, NULL, NULL, 'admin_delete', 29, 30),
(17, 10, NULL, NULL, 'admin_prefix', 31, 32),
(18, 10, NULL, NULL, 'admin_moveup', 33, 34),
(19, 10, NULL, NULL, 'admin_movedown', 35, 36),
(20, 1, NULL, NULL, 'Users', 38, 71),
(21, 20, NULL, NULL, 'admin_index', 39, 40),
(22, 20, NULL, NULL, 'admin_add', 41, 42),
(23, 20, NULL, NULL, 'admin_edit', 43, 44),
(24, 20, NULL, NULL, 'admin_reset_password', 45, 46),
(25, 20, NULL, NULL, 'admin_delete', 47, 48),
(26, 20, NULL, NULL, 'admin_login', 49, 50),
(27, 20, NULL, NULL, 'admin_logout', 51, 52),
(28, 20, NULL, NULL, 'index', 53, 54),
(29, 20, NULL, NULL, 'add', 55, 56),
(30, 20, NULL, NULL, 'activate', 57, 58),
(31, 20, NULL, NULL, 'edit', 59, 60),
(32, 20, NULL, NULL, 'forgot', 61, 62),
(33, 20, NULL, NULL, 'reset', 63, 64),
(34, 20, NULL, NULL, 'login', 65, 66),
(35, 20, NULL, NULL, 'logout', 67, 68),
(36, 20, NULL, NULL, 'view', 69, 70),
(37, 1, NULL, NULL, 'AclActions', 72, 85),
(38, 37, NULL, NULL, 'admin_index', 73, 74),
(39, 37, NULL, NULL, 'admin_add', 75, 76),
(40, 37, NULL, NULL, 'admin_edit', 77, 78),
(41, 37, NULL, NULL, 'admin_delete', 79, 80),
(42, 37, NULL, NULL, 'admin_move', 81, 82),
(43, 37, NULL, NULL, 'admin_generate', 83, 84),
(44, 1, NULL, NULL, 'AclPermissions', 86, 91),
(45, 44, NULL, NULL, 'admin_index', 87, 88),
(46, 44, NULL, NULL, 'admin_toggle', 89, 90),
(47, 1, NULL, NULL, 'ExtensionsLocales', 92, 103),
(48, 47, NULL, NULL, 'admin_index', 93, 94),
(49, 47, NULL, NULL, 'admin_activate', 95, 96),
(50, 47, NULL, NULL, 'admin_add', 97, 98),
(51, 47, NULL, NULL, 'admin_edit', 99, 100),
(52, 47, NULL, NULL, 'admin_delete', 101, 102),
(53, 1, NULL, NULL, 'ExtensionsPlugins', 104, 113),
(54, 53, NULL, NULL, 'admin_index', 105, 106),
(55, 53, NULL, NULL, 'admin_add', 107, 108),
(56, 53, NULL, NULL, 'admin_delete', 109, 110),
(57, 53, NULL, NULL, 'admin_toggle', 111, 112),
(58, 1, NULL, NULL, 'ExtensionsThemes', 114, 127),
(59, 58, NULL, NULL, 'admin_index', 115, 116),
(60, 58, NULL, NULL, 'admin_activate', 117, 118),
(61, 58, NULL, NULL, 'admin_add', 119, 120),
(62, 58, NULL, NULL, 'admin_editor', 121, 122),
(63, 58, NULL, NULL, 'admin_save', 123, 124),
(64, 58, NULL, NULL, 'admin_delete', 125, 126);

-- --------------------------------------------------------

--
-- Estrutura da tabela `areas_cursos`
--

CREATE TABLE IF NOT EXISTS `areas_cursos` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(30) NOT NULL,
  `codigo` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Extraindo dados da tabela `areas_cursos`
--

INSERT INTO `areas_cursos` (`id`, `descricao`, `codigo`) VALUES
(1, 'Área 1', '71'),
(2, 'Área 2', '72');

-- --------------------------------------------------------

--
-- Estrutura da tabela `aros`
--

CREATE TABLE IF NOT EXISTS `aros` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) DEFAULT NULL,
  `model` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `foreign_key` int(10) DEFAULT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lft` int(10) DEFAULT NULL,
  `rght` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- Extraindo dados da tabela `aros`
--

INSERT INTO `aros` (`id`, `parent_id`, `model`, `foreign_key`, `alias`, `lft`, `rght`) VALUES
(1, NULL, 'Role', 1, '', 1, 4),
(2, NULL, 'Role', 2, '', 5, 6),
(3, NULL, 'Role', 3, '', 7, 8),
(5, NULL, 'User', 1, '', 9, 10),
(6, 1, 'User', 1, NULL, 2, 3);

-- --------------------------------------------------------

--
-- Estrutura da tabela `aros_acos`
--

CREATE TABLE IF NOT EXISTS `aros_acos` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `aro_id` int(10) NOT NULL,
  `aco_id` int(10) NOT NULL,
  `_create` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `_read` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `_update` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `_delete` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Extraindo dados da tabela `aros_acos`
--

INSERT INTO `aros_acos` (`id`, `aro_id`, `aco_id`, `_create`, `_read`, `_update`, `_delete`) VALUES
(1, 3, 4, '1', '1', '1', '1'),
(2, 2, 4, '1', '1', '1', '1');

-- --------------------------------------------------------

--
-- Estrutura da tabela `campus`
--

CREATE TABLE IF NOT EXISTS `campus` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(30) NOT NULL,
  `codigo` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Extraindo dados da tabela `campus`
--

INSERT INTO `campus` (`id`, `descricao`, `codigo`) VALUES
(1, 'Marechal Deodoro', '02'),
(2, 'Maceió', '01');

-- --------------------------------------------------------

--
-- Estrutura da tabela `cursos`
--

CREATE TABLE IF NOT EXISTS `cursos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `curso_descricao_id` int(11) NOT NULL,
  `eixo_id` int(11) NOT NULL,
  `area_id` int(11) NOT NULL,
  `nivel_id` int(11) NOT NULL,
  `modalidade_id` int(11) NOT NULL,
  `turno_id` int(11) NOT NULL,
  `campus_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `codigo` varchar(50) NOT NULL,
  `data_cadastro` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `eixo_id` (`eixo_id`),
  KEY `area_id` (`area_id`),
  KEY `nivel_id` (`nivel_id`),
  KEY `modalidade_id` (`modalidade_id`),
  KEY `turno_id` (`turno_id`),
  KEY `campus_id` (`campus_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Extraindo dados da tabela `cursos`
--

INSERT INTO `cursos` (`id`, `curso_descricao_id`, `eixo_id`, `area_id`, `nivel_id`, `modalidade_id`, `turno_id`, `campus_id`, `user_id`, `codigo`, `data_cadastro`) VALUES
(4, 4, 1, 1, 1, 1, 1, 1, 1, '004100112233', '2012-09-03 16:11:15'),
(5, 4, 1, 1, 1, 1, 1, 1, 1, '5044bd87-83cc-41e8-b60a-0b84e645271b', '2012-09-03 16:24:07'),
(6, 5, 1, 1, 1, 1, 1, 1, 1, '5044c17b-221c-47bf-9d60-0b84e645271b', '2012-09-03 16:40:59');

-- --------------------------------------------------------

--
-- Estrutura da tabela `cursos_descricoes`
--

CREATE TABLE IF NOT EXISTS `cursos_descricoes` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `descricao` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Extraindo dados da tabela `cursos_descricoes`
--

INSERT INTO `cursos_descricoes` (`id`, `nome`, `descricao`) VALUES
(4, 'Curso 1', 'Curso 1'),
(5, 'Curso 2', 'Curso 2');

-- --------------------------------------------------------

--
-- Estrutura da tabela `eixos_cursos`
--

CREATE TABLE IF NOT EXISTS `eixos_cursos` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(30) NOT NULL,
  `codigo` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Extraindo dados da tabela `eixos_cursos`
--

INSERT INTO `eixos_cursos` (`id`, `descricao`, `codigo`) VALUES
(1, 'Eixo 1', '01'),
(2, 'Eixo 2', '21');

-- --------------------------------------------------------

--
-- Estrutura da tabela `etnias`
--

CREATE TABLE IF NOT EXISTS `etnias` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `etnias`
--

INSERT INTO `etnias` (`id`, `descricao`) VALUES
(1, 'pardo');

-- --------------------------------------------------------

--
-- Estrutura da tabela `exames`
--

CREATE TABLE IF NOT EXISTS `exames` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `descricao` varchar(255) NOT NULL,
  `data_inicio_inscricao_normal` date NOT NULL,
  `data_fim_inscricao_normal` date NOT NULL,
  `data_inicio_inscricao_isento` date NOT NULL,
  `data_fim_inscricao_isento` date NOT NULL,
  `data_exame` date NOT NULL,
  `taxa_inscricao` decimal(10,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `habilitacoes`
--

CREATE TABLE IF NOT EXISTS `habilitacoes` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `habilitacoes`
--

INSERT INTO `habilitacoes` (`id`, `descricao`) VALUES
(1, 'teste');

-- --------------------------------------------------------

--
-- Estrutura da tabela `i18n`
--

CREATE TABLE IF NOT EXISTS `i18n` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `locale` varchar(6) COLLATE utf8_unicode_ci NOT NULL,
  `model` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `foreign_key` int(10) NOT NULL,
  `field` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `locale` (`locale`),
  KEY `model` (`model`),
  KEY `row_id` (`foreign_key`),
  KEY `field` (`field`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `linguas`
--

CREATE TABLE IF NOT EXISTS `linguas` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `modalidades_cursos`
--

CREATE TABLE IF NOT EXISTS `modalidades_cursos` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(30) NOT NULL,
  `codigo` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Extraindo dados da tabela `modalidades_cursos`
--

INSERT INTO `modalidades_cursos` (`id`, `descricao`, `codigo`) VALUES
(1, 'Modalidade 1', '21'),
(2, 'Modalidade 2', '22');

-- --------------------------------------------------------

--
-- Estrutura da tabela `necessidades_especiais`
--

CREATE TABLE IF NOT EXISTS `necessidades_especiais` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `necessidades_especiais`
--

INSERT INTO `necessidades_especiais` (`id`, `descricao`) VALUES
(1, 'teste');

-- --------------------------------------------------------

--
-- Estrutura da tabela `niveis_cursos`
--

CREATE TABLE IF NOT EXISTS `niveis_cursos` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(30) NOT NULL,
  `codigo` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Extraindo dados da tabela `niveis_cursos`
--

INSERT INTO `niveis_cursos` (`id`, `descricao`, `codigo`) VALUES
(1, 'Médio', '01'),
(2, 'Subsequente', '02');

-- --------------------------------------------------------

--
-- Estrutura da tabela `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `role_alias` (`alias`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Extraindo dados da tabela `roles`
--

INSERT INTO `roles` (`id`, `title`, `alias`, `created`, `updated`) VALUES
(1, 'Admin', 'admin', '2009-04-05 00:10:34', '2009-04-05 00:10:34'),
(2, 'Registrado', 'registrado', '2009-04-05 00:10:50', '2012-07-16 06:13:02'),
(3, 'Público', 'publico', '2009-04-05 00:12:38', '2012-07-16 06:13:12');

-- --------------------------------------------------------

--
-- Estrutura da tabela `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `key` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `input_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'text',
  `editable` tinyint(1) NOT NULL DEFAULT '1',
  `weight` int(11) DEFAULT NULL,
  `params` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key` (`key`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=35 ;

--
-- Extraindo dados da tabela `settings`
--

INSERT INTO `settings` (`id`, `key`, `value`, `title`, `description`, `input_type`, `editable`, `weight`, `params`) VALUES
(6, 'Site.title', 'Croogo', '', '', '', 1, 1, ''),
(7, 'Site.tagline', 'A CakePHP powered Content Management System.', '', '', 'textarea', 1, 2, ''),
(8, 'Site.email', 'you@your-site.com', '', '', '', 1, 3, ''),
(9, 'Site.status', '1', '', '', 'checkbox', 1, 5, ''),
(12, 'Meta.robots', 'index, follow', '', '', '', 1, 6, ''),
(13, 'Meta.keywords', 'croogo, Croogo', '', '', 'textarea', 1, 7, ''),
(14, 'Meta.description', 'Croogo - A CakePHP powered Content Management System', '', '', 'textarea', 1, 8, ''),
(15, 'Meta.generator', 'Croogo - Content Management System', '', '', '', 0, 9, ''),
(16, 'Service.akismet_key', 'your-key', '', '', '', 1, 11, ''),
(17, 'Service.recaptcha_public_key', 'your-public-key', '', '', '', 1, 12, ''),
(18, 'Service.recaptcha_private_key', 'your-private-key', '', '', '', 1, 13, ''),
(19, 'Service.akismet_url', 'http://your-blog.com', '', '', '', 1, 10, ''),
(20, 'Site.theme', '', '', '', '', 0, 14, ''),
(21, 'Site.feed_url', '', '', '', '', 0, 15, ''),
(22, 'Reading.nodes_per_page', '5', '', '', '', 1, 16, ''),
(23, 'Writing.wysiwyg', '1', 'Enable WYSIWYG editor', '', 'checkbox', 1, 17, ''),
(24, 'Comment.level', '1', '', 'levels deep (threaded comments)', '', 1, 18, ''),
(25, 'Comment.feed_limit', '10', '', 'number of comments to show in feed', '', 1, 19, ''),
(26, 'Site.locale', 'eng', '', '', 'text', 0, 20, ''),
(27, 'Reading.date_time_format', 'D, M d Y H:i:s', '', '', '', 1, 21, ''),
(28, 'Comment.date_time_format', 'M d, Y', '', '', '', 1, 22, ''),
(29, 'Site.timezone', '0', '', 'zero (0) for GMT', '', 1, 4, ''),
(32, 'Hook.bootstraps', 'Tinymce', '', '', '', 0, 23, ''),
(33, 'Comment.email_notification', '1', 'Enable email notification', '', 'checkbox', 1, 24, ''),
(34, 'Croogo.version', '1.4.1', '', '', '', 0, 25, '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tipos_anexos`
--

CREATE TABLE IF NOT EXISTS `tipos_anexos` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Extraindo dados da tabela `tipos_anexos`
--

INSERT INTO `tipos_anexos` (`id`, `descricao`) VALUES
(1, 'Aviso'),
(2, 'Anexo');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tipos_escola_origem`
--

CREATE TABLE IF NOT EXISTS `tipos_escola_origem` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Extraindo dados da tabela `tipos_escola_origem`
--

INSERT INTO `tipos_escola_origem` (`id`, `descricao`) VALUES
(1, 'Cenecista'),
(2, 'Municipal'),
(3, 'Estadual'),
(4, 'Federal');

-- --------------------------------------------------------

--
-- Estrutura da tabela `turnos`
--

CREATE TABLE IF NOT EXISTS `turnos` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(30) NOT NULL,
  `codigo` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Extraindo dados da tabela `turnos`
--

INSERT INTO `turnos` (`id`, `descricao`, `codigo`) VALUES
(1, 'Matutino', '81'),
(2, 'Vespertino', '82');

-- --------------------------------------------------------

--
-- Estrutura da tabela `types`
--

CREATE TABLE IF NOT EXISTS `types` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `format_show_author` tinyint(1) NOT NULL DEFAULT '1',
  `format_show_date` tinyint(1) NOT NULL DEFAULT '1',
  `comment_status` int(1) NOT NULL DEFAULT '1',
  `comment_approve` tinyint(1) NOT NULL DEFAULT '1',
  `comment_spam_protection` tinyint(1) NOT NULL DEFAULT '0',
  `comment_captcha` tinyint(1) NOT NULL DEFAULT '0',
  `params` text COLLATE utf8_unicode_ci,
  `plugin` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated` datetime NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `type_alias` (`alias`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Extraindo dados da tabela `types`
--

INSERT INTO `types` (`id`, `title`, `alias`, `description`, `format_show_author`, `format_show_date`, `comment_status`, `comment_approve`, `comment_spam_protection`, `comment_captcha`, `params`, `plugin`, `updated`, `created`) VALUES
(1, 'Page', 'page', 'A page is a simple method for creating and displaying information that rarely changes, such as an "About us" section of a website. By default, a page entry does not allow visitor comments.', 0, 0, 0, 1, 0, 0, '', '', '2009-09-09 00:23:24', '2009-09-02 18:06:27'),
(2, 'Blog', 'blog', 'A blog entry is a single post to an online journal, or blog.', 1, 1, 2, 1, 0, 0, '', '', '2009-09-15 12:15:43', '2009-09-02 18:20:44'),
(4, 'Node', 'node', 'Default content type.', 1, 1, 2, 1, 0, 0, '', '', '2009-10-06 21:53:15', '2009-09-05 23:51:56');

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL,
  `username` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `website` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `activation_key` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bio` text COLLATE utf8_unicode_ci,
  `timezone` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `updated` datetime NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `users`
--

INSERT INTO `users` (`id`, `role_id`, `username`, `password`, `name`, `email`, `website`, `activation_key`, `image`, `bio`, `timezone`, `status`, `updated`, `created`) VALUES
(1, 1, 'admin', 'a00948366e95fd68d80448f60dd4785fa0c37f69', 'admin', '', NULL, 'f1eea620a88042fc02ae71dcbb528f7f', NULL, NULL, '0', 1, '2012-07-16 05:30:07', '2012-07-16 05:30:07');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
