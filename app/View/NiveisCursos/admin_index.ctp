<?php //$this->extend('/Common/admin_index'); ?>

<div class="niveis_cursos index simplebox">
	 <div class="title">
            <?php echo $title_for_layout; ?>
            <span></span>
        </div>
        <div class="body">

            <div class="actions">
                    <ul>
                            <li><?php echo $this->Html->link(__('Novo Nível de Curso', true), array('action'=>'add')); ?></li>
                    </ul>
            </div>

            <table cellpadding="0" cellspacing="0">
            <?php
                    $tableHeaders =  $this->Html->tableHeaders(array(
                            $this->Paginator->sort('codigo','Código'),
                            $this->Paginator->sort('descricao','Descrição'),
                            __('Ações', true),
                    ));
                    echo $tableHeaders;

                    $rows = array();
                    foreach ($niveis AS $nivel) {
                            $actions  = $this->Html->link(__('Editar', true), array('controller' => 'niveis_cursos', 'action' => 'edit', $nivel['NivelCurso']['id']));
                            $actions .= ' ' . $this->Layout->adminRowActions($nivel['NivelCurso']['id']);
                            $actions .= ' ' . $this->Html->link(__('Excluir', true), array(
                                    'controller' => 'niveis_cursos',
                                    'action' => 'delete',
                                    $nivel['NivelCurso']['id'],
                                    'token' => $this->params['_Token']['key'],
                            ), null, __('Are you sure?', true));

                            $rows[] = array(
                                    h($nivel['NivelCurso']['codigo']),
                                    h($nivel['NivelCurso']['descricao']),
                                    $actions,
                            );
                    }

                    echo $this->Html->tableCells($rows);
            ?>
            </table>
        <div class="paging">
            <?php echo $this->Paginator->prev('');?>
            <?php echo $this->Paginator->numbers(array('separator' => '')); ?>
            <?php echo $this->Paginator->next('');?>
        </div>
    </div>
</div>
