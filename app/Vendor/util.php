<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of util
 *
 * @author Uziel
 */
class Util {

    public function converteData($data){
        $data_nova = implode(preg_match("~\/~", $data) == 0 ? "/" : "-", array_reverse(explode(preg_match("~\/~", $data) == 0 ? "-" : "/", $data)));
        return $data_nova;
    }

    public function validaData($data) {
        $data = split("[-,/]", $data);
        if(!checkdate($data[1], $data[0], $data[2]) and !checkdate($data[1], $data[2], $data[0])) {
                return false;
        }
        return true;
    }

    public function formataDinheiro($valor){
        $valor = str_replace('.', '', $valor);
        return number_format($valor, 2, '.', '');
    }
    
}
?>
