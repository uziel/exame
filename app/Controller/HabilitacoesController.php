<?php
/**
 * Habilitacoes Controller
 *
 * PHP version 5
 *
 * @category Controller
 * @package  Exame IFAL
 * @version  1.0
 * @author   Uziel Barbosa <contato@uzielbarbosa.com.br>
 * @license  http://www.opensource.org/licenses/mit-license.php The MIT License
 * @link     http://exame.ifal.edu.br
 */
class HabilitacoesController extends AppController {

/**
 * Controller name
 *
 * @var string
 * @access public
 */
	public $name = 'Habilitacoes';

/**
 * Models used by the Controller
 *
 * @var array
 * @access public
 */
	public $uses = array('Habilitacao');

	public function admin_index() {
		$this->set('title_for_layout', __('Habilitações'));

		$this->Habilitacao->recursive = 0;
		$this->paginate['Habilitacao']['order'] = "Habilitacao.id ASC";
		$this->set('habilitacoes', $this->paginate());
		$this->set('displayFields', $this->Habilitacao->displayFields());
	}

	public function admin_add() {
		$this->set('title_for_layout', __('Adicionar Habilitação'));

		if (!empty($this->request->data)) {
			$this->Habilitacao->create();
			if ($this->Habilitacao->save($this->request->data)) {
				$this->Session->setFlash(__('Habilitação cadastrada com sucesso'), 'default', array('class' => 'success'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Erro ao cadastrar habilitação, tente novamente'), 'default', array('class' => 'error'));
			}
		}
	}

	public function admin_edit($id = null) {
		$this->set('title_for_layout', __('Editar Habilitação'));

		if (!$id && empty($this->request->data)) {
			$this->Session->setFlash(__('Habilitação inválida'), 'default', array('class' => 'error'));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->request->data)) {
			if ($this->Habilitacao->save($this->request->data)) {
				$this->Session->setFlash(__('Habilitação alterada com sucesso'), 'default', array('class' => 'success'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Erro ao alterar habilitação, tente novamente'), 'default', array('class' => 'error'));
			}
		}
		if (empty($this->request->data)) {
			$this->request->data = $this->Habilitacao->read(null, $id);
		}
	}

	public function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Id da Habilitação inválido'), 'default', array('class' => 'error'));
			$this->redirect(array('action' => 'index'));
		}
		if ($this->Habilitacao->delete($id)) {
			$this->Session->setFlash(__('Habilitação excluída com sucesso'), 'default', array('class' => 'success'));
			$this->redirect(array('action' => 'index'));
		}
	}

}
