<?php
/**
 * CursoNome Controller
 *
 * PHP version 5
 *
 * @category Controller
 * @package  Exame IFAL
 * @version  1.0
 * @author   Uziel Barbosa <contato@uzielbarbosa.com.br>
 * @license  http://www.opensource.org/licenses/mit-license.php The MIT License
 * @link     http://exame.ifal.edu.br
 */
class CursosNomesController extends AppController {

/**
 * Controller name
 *
 * @var string
 * @access public
 */
	public $name = 'CursosNomes';

/**
 * Models used by the Controller
 *
 * @var array
 * @access public
 */
	public $uses = array('CursoNome');

	public function admin_index() {
		$this->set('title_for_layout', __('Cursos'));

		$this->CursoNome->recursive = 0;
		$this->paginate['CursoNome']['order'] = "CursoNome.nome ASC";
		$this->set('cursos', $this->paginate());
	}

	public function admin_add() {
		$this->set('title_for_layout', __('Adicionar Curso'));

		if (!empty($this->request->data)) {
			$this->CursoNome->create();
                        
			if ($this->CursoNome->save($this->request->data)) {
				$this->Session->setFlash(__('Curso cadastrado com sucesso'), 'default', array('class' => 'success'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Erro ao cadastrar curso, tente novamente'), 'default', array('class' => 'error'));
			}
		}
                $this->carregarDadosCombobox('TipoCurso', 'tiposCursos');
	}

	public function admin_edit($id = null) {
		$this->set('title_for_layout', __('Editar Curso'));

		if (!$id && empty($this->request->data)) {
			$this->Session->setFlash(__('Curso inválido'), 'default', array('class' => 'error'));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->request->data)) {
			if ($this->CursoNome->save($this->request->data)) {
				$this->Session->setFlash(__('Curso alterado com sucesso'), 'default', array('class' => 'success'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Erro ao alterar curso, tente novamente'), 'default', array('class' => 'error'));
			}
		}
		if (empty($this->request->data)) {
			$this->request->data = $this->CursoNome->read(null, $id);
		}
                $this->carregarDadosCombobox('TipoCurso', 'tiposCursos');
	}

	public function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Id do curso inválido'), 'default', array('class' => 'error'));
			$this->redirect(array('action' => 'index'));
		}
		if ($this->CursoNome->delete($id)) {
			$this->Session->setFlash(__('Curso excluído com sucesso'), 'default', array('class' => 'success'));
			$this->redirect(array('action' => 'index'));
		}
	}

}
