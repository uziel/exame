<?php $this->extend('/Common/admin_edit'); ?>
<?php echo $this->Form->create('NivelCurso');?>

    <?php
            echo $this->Form->input('codigo', array ('label' => 'Código', 'class' => 'input-big'));
            echo $this->Form->input('descricao', array ('label' => 'Descrição', 'class' => 'input-big'));
    ?>
		
    <div class="clear"></div>
    
    <div class="buttons">
    <?php
            echo $this->Form->end(__('Salvar'));
            echo $this->Html->link(__('Cancelar'), array(
                    'action' => 'index',
            ), array(
                    'class' => 'cancel',
            ));
    ?>
    </div>
    </div>
</div>