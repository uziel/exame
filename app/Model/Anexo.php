<?php
/**
 * Anexo
 *
 * PHP version 5
 *
 * @category Model
 * @package  Exame IFAL
 * @version  1.0
 * @author   Uziel Barbosa <contato@uzielbarbosa.com.br>
 * @license  http://www.opensource.org/licenses/mit-license.php The MIT License
 * @link     http://www.croogo.org
 */
class Anexo extends AppModel {

/**
 * Model name
 *
 * @var string
 * @access public
 */
	public $name = 'Anexo';
        
        public $useTable = 'anexos';



/**
 * Validation
 *
 * @var array
 * @access public
 */
	public $validate = array(
                'descricao' => array(
			'notEmpty' => array(
				'rule' => 'notEmpty',
				'message' => 'O campo deve ser informado.',
			),
		),
	);

/**
 * Display fields for this model
 *
 * @var array
 */
	protected $_displayFields = array(
		'id',
		'descricao',
                'TipoAnexo.descricao' => 'Tipo',
	);
        
        
        
   public $belongsTo = array(
		'Exame' => array(
			'className' => 'Exame',
			'foreignKey' => 'exame_id',
			'conditions' => '',
			'fields' => '',
			'order' => '',
		),
                'TipoAnexo' => array(
			'className' => 'TipoAnexo',
			'foreignKey' => 'tipo_anexo_id',
			'conditions' => '',
			'fields' => '',
			'order' => '',
		),
	);

}
