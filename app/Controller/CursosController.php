<?php
/**
 * Curso Controller
 *
 * PHP version 5
 *
 * @category Controller
 * @package  Exame IFAL
 * @version  1.0
 * @author   Uziel Barbosa <contato@uzielbarbosa.com.br>
 * @license  http://www.opensource.org/licenses/mit-license.php The MIT License
 * @link     http://exame.ifal.edu.br
 */
class CursosController extends AppController {

/**
 * Controller name
 *
 * @var string
 * @access public
 */
	public $name = 'Cursos';

/**
 * Models used by the Controller
 *
 * @var array
 * @access public
 */
	public $uses = array('Curso');

	public function admin_index() {
		$this->set('title_for_layout', __('Cursos'));

		$this->Curso->recursive = 0;
		$this->paginate['Curso']['order'] = "Curso.id ASC";
		$this->set('cursos', $this->paginate());
		$this->set('displayFields', $this->Curso->displayFields());
	}

	public function admin_add() {
		$this->set('title_for_layout', __('Adicionar Curso'));

		if (!empty($this->request->data)) {
			$this->Curso->create();
                        
                        $this->request->data['Curso']['data_cadastro'] = date('Y-m-d H:i:s');
                        $this->request->data['Curso']['user_id'] = $this->Session->read('Auth.User.id');
                        if ($this->Curso->save($this->request->data)) {
                                $this->request->data['Curso']['id'] = $this->Curso->id;
                                $this->gerarCodigoCurso();
                                $this->Session->setFlash(__('Curso cadastrado com sucesso'), 'default', array('class' => 'success'));
                                $this->redirect(array('action' => 'index'));
                        } else {
                                $this->Session->setFlash(__('Erro ao cadastrar curso, tente novamente'), 'default', array('class' => 'error'));
                                pr($this->Curso->invalidFields());
                                break;

                        }
		}
                
                $this->carregarDadosAddEditCurso();
                
	}
        
        private function gerarCodigoCurso() {
            
            $codigo = $this->request->data['Curso']['id'] .
                    $this->request->data['Curso']['nivel_id'] .
                    $this->request->data['Curso']['nivel_id'] .
                    $this->request->data['Curso']['campus_id'] .
                    $this->request->data['Curso']['curso_nome_id'] .
                    $this->request->data['Curso']['turno_id'];
            
           $this->Curso->saveField('codigo', $codigo);
        }

	public function admin_edit($id = null) {
		$this->set('title_for_layout', __('Editar Curso'));

		if (!$id && empty($this->request->data)) {
			$this->Session->setFlash(__('Curso inválida'), 'default', array('class' => 'error'));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->request->data)) {
			if ($this->Curso->save($this->request->data)) {
                                $this->gerarCodigoCurso();
				$this->Session->setFlash(__('Curso alterado com sucesso'), 'default', array('class' => 'success'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Erro ao alterar curso, tente novamente'), 'default', array('class' => 'error'));
			}
		}
		if (empty($this->request->data)) {
			$this->request->data = $this->Curso->read(null, $id);
		}
                $this->carregarDadosAddEditCurso();
	}
        
        private function carregarDadosAddEditCurso() {
            $this->carregarDadosCombobox('CursoNome','cursos', array('id', 'nome'));
            $this->carregarDadosCombobox('Campus','campus');
            $this->carregarDadosCombobox('NivelCurso','niveis');
            $this->carregarDadosCombobox('Turno','turnos');
        }

	public function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Id do Curso inválido'), 'default', array('class' => 'error'));
			$this->redirect(array('action' => 'index'));
		}
		if ($this->Curso->delete($id)) {
			$this->Session->setFlash(__('Curso excluído com sucesso'), 'default', array('class' => 'success'));
			$this->redirect(array('action' => 'index'));
		}
	}

}
