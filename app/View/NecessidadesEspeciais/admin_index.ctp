<?php //$this->extend('/Common/admin_index'); ?>

<div class="necessidades_especiais index">
	<h2><?php echo $title_for_layout; ?></h2>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('Nova Necessidade Especial', true), array('action'=>'add')); ?></li>
		</ul>
	</div>

	<table cellpadding="0" cellspacing="0">
	<?php
		$tableHeaders =  $this->Html->tableHeaders(array(
			$this->Paginator->sort('id'),
			$this->Paginator->sort('descricao','Descrição'),
			__('Ações', true),
		));
		echo $tableHeaders;

		$rows = array();
		foreach ($necessidades AS $necessidade) {
			$actions  = $this->Html->link(__('Editar', true), array('controller' => 'necessidades_especiais', 'action' => 'edit', $necessidade['NecessidadeEspecial']['id']));
			$actions .= ' ' . $this->Layout->adminRowActions($necessidade['NecessidadeEspecial']['id']);
			$actions .= ' ' . $this->Html->link(__('Excluir', true), array(
				'controller' => 'necessidades_especiais',
				'action' => 'delete',
				$necessidade['NecessidadeEspecial']['id'],
				'token' => $this->params['_Token']['key'],
			), null, __('Are you sure?', true));

			$rows[] = array(
				$necessidade['NecessidadeEspecial']['id'],
				h($necessidade['NecessidadeEspecial']['descricao']),
				$actions,
			);
		}

		echo $this->Html->tableCells($rows);
		echo $tableHeaders;
	?>
	</table>
</div>

<div class="paging"><?php echo $this->Paginator->numbers(); ?></div>
<div class="counter"><?php echo $this->Paginator->counter(array('format' => __('Página %page% de %pages%, mostrando %current% registro de %count% total'))); ?></div>

