<?php $this->Html->script(array('jquery/jquery.maskedinput'), false); ?>

<?php $this->extend('/Common/admin_edit'); ?>
<?php echo $this->Form->create('Exame');?>

    <?php
            echo $this->Form->input('id');
            echo $this->Form->input('nome', array ('label' => 'Nome', 'class' => 'input-big'));
            echo $this->Form->input('descricao', array ('label' => 'Descrição*', 'class' => 'input-big'));
            echo $this->Form->input('taxa_inscricao', array ('label' => 'Taxa de inscrição*', 'class' => 'input-big'));
            echo $this->Form->input('data_inicio_inscricao_normal', array ('label' => 'Data de início da inscrição normal*', 'class' => 'input-big', 'type' => 'text'));
            echo $this->Form->input('data_fim_inscricao_normal', array ('label' => 'Data final da inscrição normal*', 'class' => 'input-big', 'type' => 'text'));
            echo $this->Form->input('data_inicio_inscricao_isento', array ('label' => 'Data de início da inscrição isento*', 'class' => 'input-big', 'type' => 'text'));
            echo $this->Form->input('data_fim_inscricao_isento', array ('label' => 'Data final da inscrição isento*', 'class' => 'input-big', 'type' => 'text'));
            echo $this->Form->input('data_exame', array ('label' => 'Data do exame*', 'class' => 'input-big', 'type' => 'text'));
    ?>
		
    <div class="clear"></div>
    
    <div class="buttons">
    <?php
            echo $this->Form->end(__('Salvar'));
            echo $this->Html->link(__('Cancelar'), array(
                    'action' => 'index',
            ), array(
                    'class' => 'cancel',
            ));
    ?>
    </div>
    </div>
</div>


<style type="text/css">
    .input {
        min-height: 71px;
    }
</style>


<script type="text/javascript">
    
    jQuery(function($){
       $("#EnderecoCep").mask("99999-999");
       
       $("#ExameDataInicioInscricaoNormal").mask("99/99/9999").datepicker({dateFormat: "dd/mm/yy"});
       $("#ExameDataFimInscricaoNormal").mask("99/99/9999").datepicker({dateFormat: "dd/mm/yy"});
       $("#ExameDataInicioInscricaoIsento").mask("99/99/9999").datepicker({dateFormat: "dd/mm/yy"});
       $("#ExameDataFimInscricaoIsento").mask("99/99/9999").datepicker({dateFormat: "dd/mm/yy"});
       $("#ExameDataExame").mask("99/99/9999").datepicker({dateFormat: "dd/mm/yy"});
       
    });
    
</script>