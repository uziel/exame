<?php //$this->extend('/Common/admin_index'); ?>

<div class="exames index simplebox">
	 <div class="title">
            <?php echo $title_for_layout; ?>
            <span></span>
        </div>
        <div class="body">

            <div class="painel">
                <div class="resumo">
                    <div class="item">
                        <span class="num"><a href="#">5500</a></span>
                        <span class="descricao">Inscritos</span>
                    </div>
                    <div class="item">
                        <span class="num green"><a href="#">1500</a></span>
                        <span class="descricao green">Confirmados</span>
                    </div>
                    <div class="item">
                        <span class="num"><a href="#">3000</a></span>
                        <span class="descricao">Não confirmados</span>
                    </div>
                    <div class="item">
                        <span class="num red"><a href="#">1000</a></span>
                        <span class="descricao red">Cancelados</span>
                    </div>
                </div>
                
                <h3>Ofertas</h3>
                
                <table cellpadding="0" cellspacing="0">
            <?php
                    $tableHeaders =  $this->Html->tableHeaders(array(
                            'Código',
                            'Curso',
                            'Campus',
                            'Turno',
                            __('Ações', true),
                    ));
                    echo $tableHeaders;
                    $rows = array(
                            '11223344',
                            'Informatica',
                            'Maceio',
                            'Vespertino',
                            'Editar | Excluir'
                    );

                    echo $this->Html->tableCells($rows);
                    echo $this->Html->tableCells($rows);
                    echo $this->Html->tableCells($rows);
            ?>
            </table>
                
                <h3>Opções</h3>
                
                <div class="opcoes">
                    <div class="item">
                        <div class="imagem"></div>
                        <span>
                            <a href="<?php echo $this->base;?>/admin/avisos/index/<?php echo $exame['Exame']['id'];?>">Adicionar Aviso</a>
                        </span>
                    </div>
                    
                    <div class="item">
                        <div class="imagem"></div>
                        <span>
                            <a href="<?php echo $this->base;?>/admin/anexos/index/<?php echo $exame['Exame']['id'];?>">Adicionar Anexo</a>
                        </span>
                    </div>
                    
                    <div class="item">
                        <div class="imagem"></div>
                        <span>
                            <a href="<?php echo $this->base;?>/admin/cursos/index/<?php echo $exame['Exame']['id'];?>">Adicionar Oferta</a>
                        </span>
                    </div>
                    
                </div>
            </div>
            
            
    </div>
</div>
