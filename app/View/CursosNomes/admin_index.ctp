<?php //$this->extend('/Common/admin_index'); ?>

<div class="cursos index simplebox">
	 <div class="title">
            <?php echo $title_for_layout; ?>
            <span></span>
        </div>
        <div class="body">

            <div class="actions">
                    <ul>
                            <li><?php echo $this->Html->link(__('Novo Curso', true), array('action'=>'add')); ?></li>
                    </ul>
            </div>

            <table cellpadding="0" cellspacing="0">
            <?php
                    $tableHeaders =  $this->Html->tableHeaders(array(
                            $this->Paginator->sort('id','Código'),
                            $this->Paginator->sort('nome','Nome'),
                            $this->Paginator->sort('tipo_curso_id','Tipo'),
                            __('Ações', true),
                    ));
                    echo $tableHeaders;

                    $rows = array();
                    foreach ($cursos AS $curso) {
                            $actions  = $this->Html->link(__('Editar', true), array('controller' => 'cursos_nomes', 'action' => 'edit', $curso['CursoNome']['id']));
                            $actions .= ' ' . $this->Layout->adminRowActions($curso['CursoNome']['id']);
                            $actions .= ' ' . $this->Html->link(__('Excluir', true), array(
                                    'controller' => 'cursos_nomes',
                                    'action' => 'delete',
                                    $curso['CursoNome']['id'],
                                    'token' => $this->params['_Token']['key'],
                            ), null, __('Are you sure?', true));

                            $rows[] = array(
                                    h($curso['CursoNome']['id']),
                                    h($curso['CursoNome']['nome']),
                                    h($curso['TipoCurso']['descricao']),
                                    $actions,
                            );
                    }

                    echo $this->Html->tableCells($rows);
            ?>
            </table>
        <div class="paging">
            <?php echo $this->Paginator->prev('');?>
            <?php echo $this->Paginator->numbers(array('separator' => '')); ?>
            <?php echo $this->Paginator->next('');?>
        </div>
    </div>
</div>
