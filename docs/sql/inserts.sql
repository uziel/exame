
--
-- Dados da tabela `tipos_anexos`
--

INSERT INTO `tipos_anexos` (`id`, `descricao`) VALUES
(1, 'Aviso'),
(2, 'Anexo');

INSERT INTO `tipos_escola_origem` (`id`, `descricao`) VALUES
(1, 'Cenecista'),
(2, 'Municipal'),
(3, 'Estadual'),
(4, 'Federal');