<?php //$this->extend('/Common/admin_index'); ?>

<div class="users index simplebox">
	 <div class="title">
            <?php echo $title_for_layout; ?>
            <span></span>
        </div>
        <div class="body">

            <div class="actions">
                    <ul>
                            <li><?php echo $this->Html->link(__('Novo usuário', true), array('action'=>'add')); ?></li>
                    </ul>
            </div>

            <table cellpadding="0" cellspacing="0">
            <?php
                    $tableHeaders =  $this->Html->tableHeaders(array(
                            $this->Paginator->sort('id','Código'),
                            $this->Paginator->sort('Role.title','Perfil'),
                            $this->Paginator->sort('username','Usuário'),
                            $this->Paginator->sort('name','Nome'),
                            $this->Paginator->sort('status','Status'),
                            $this->Paginator->sort('email','E-mail'),
                            __('Ações', true),
                    ));
                    echo $tableHeaders;

                    $rows = array();
                    foreach ($users AS $user) {
                            $actions = $this->Html->link(__('Editar', true), array('controller' => 'users', 'action' => 'edit', $user['User']['id']));
                            $actions .= ' ' . $this->Layout->adminRowActions($user['User']['id']);
                            $actions .= ' ' . $this->Html->link(__('Excluir', true), array(
                                    'controller' => 'users',
                                    'action' => 'delete',
                                    $user['User']['id'],
                                    'token' => $this->params['_Token']['key'],
                            ), null, __('Are you sure?', true));

                            $rows[] = array(
                                    $user['User']['id'],
                                    h($user['Role']['title']),
                                    h($user['User']['username']),
                                    h($user['User']['name']),
                                    $this->Layout->status($user['User']['status']),
                                    $user['User']['email'],
                                    $actions,
                            );
                    }

                    echo $this->Html->tableCells($rows);
            ?>
            </table>
        <div class="paging">
            <?php echo $this->Paginator->prev('');?>
            <?php echo $this->Paginator->numbers(array('separator' => '')); ?>
            <?php echo $this->Paginator->next('');?>
        </div>
    </div>
</div>
