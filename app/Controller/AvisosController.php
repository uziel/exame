    <?php
/**
 * Aviso Controller
 *
 * PHP version 5
 *
 * @category Controller
 * @package  Aviso IFAL
 * @version  1.0
 * @author   Uziel Barbosa <contato@uzielbarbosa.com.br>
 * @license  http://www.opensource.org/licenses/mit-license.php The MIT License
 * @link     http://aviso.ifal.edu.br
 */
class AvisosController extends AppController {

/**
 * Controller name
 *
 * @var string
 * @access public
 */
	public $name = 'Avisos';

/**
 * Models used by the Controller
 *
 * @var array
 * @access public
 */
	public $uses = array('Aviso');

	public function admin_index($id = null) {
            
            if(!$id) {
                $this->Session->setFlash(__('O exame não foi selecionado'), 'default', array('class' => 'error'));
                $this->redirect(array('action' => 'index'));
            }
            
            $this->Aviso->recursive = 0;
            $exame = $this->Aviso->Exame->read(null, $id);
            
            $this->set('title_for_layout', 'Avisos de: ' . $exame['Exame']['nome']);

            $this->Aviso->recursive = 0;
            $this->paginate['Aviso']['conditions'] = array('Aviso.exame_id' => $id);
            $this->paginate['Aviso']['order'] = "Aviso.id ASC";
            $this->set('avisos', $this->paginate());
            
            
            $this->set(compact('exame'));
            
	}
        
        
        public function admin_painel($id  = null) {
            
            if(!$id) {
                $this->Session->setFlash(__('O aviso não foi selecionado'), 'default', array('class' => 'error'));
                $this->redirect(array('action' => 'index'));
            }
            
            $this->Aviso->recursive = 0;
            $aviso = $this->Aviso->read(null, $id);
            
            $this->set('title_for_layout', $aviso['Aviso']['nome']);
            
            $this->set(compact('aviso'));
	}

	public function admin_add() {
		$this->set('title_for_layout', __('Adicionar Aviso'));

		if (!empty($this->request->data)) {
			$this->Aviso->create();
                        
                        $this->request->data['Aviso']['data_cadastro'] = date('Y-m-d H:i:s');
                        $this->request->data['Aviso']['user_id'] = $this->Session->read('Auth.User.id');
                        
                        $this->request->data['Aviso']['codigo'] = '004100112233';
                        
			if ($this->Aviso->save($this->request->data)) {
				$this->Session->setFlash(__('Aviso cadastrado com sucesso'), 'default', array('class' => 'success'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Erro ao cadastrar aviso, tente novamente'), 'default', array('class' => 'error'));
                                //pr($this->Aviso->invalidFields()); break;
			}
		}
                
	}
        
        public function admin_ofertas($exameId = null) {
		$this->set('title_for_layout', __('Ofertar Curso'));
                
                if(!$exameId) {
                    $this->Session->setFlash(__('Aviso inválido'), 'default', array('class' => 'error'));
                    $this->redirect(array('action' => 'index'));
                }

		if (!empty($this->request->data)) {
                    
			$this->ExameCurso->create();
                        
                        $this->request->data['ExameCurso']['data_cadastro'] = date('Y-m-d H:i:s');
                        $this->request->data['ExameCurso']['user_id'] = $this->Session->read('Auth.User.id');
                        
                        $this->request->data['ExameCurso']['codigo'] = '004100112233';
                        
			if ($this->ExameCurso->save($this->request->data)) {
				$this->Session->setFlash(__('Oferta cadastrada com sucesso'), 'default', array('class' => 'success'));
				$this->redirect(array('action' => 'ofertas', $this->request->data['ExameCurso']['exame_id']));
			} else {
				$this->Session->setFlash(__('Erro ao cadastrar oferta, tente novamente'), 'default', array('class' => 'error'));
			}
		}
                
                $cursos = $this->Curso->find('all', array('fields' => array('id', 'CursoDescricao.descricao')));
                
                $this->ExameCurso->recursive = 2;
                $condicoes = array('ExameCurso.exame_id' => $exameId);
                $exameCursos = $this->ExameCurso->find('all', array('conditions' => $condicoes));
                
                $this->set(compact('exameId','cursos','exameCursos'));
                
	}

	public function admin_edit($id = null) {
		$this->set('title_for_layout', __('Editar Aviso'));

		if (!$id && empty($this->request->data)) {
			$this->Session->setFlash(__('Aviso inválida'), 'default', array('class' => 'error'));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->request->data)) {
			if ($this->Aviso->save($this->request->data)) {
				$this->Session->setFlash(__('Aviso alterado com sucesso'), 'default', array('class' => 'success'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Erro ao alterar campus, tente novamente'), 'default', array('class' => 'error'));
			}
		}
		if (empty($this->request->data)) {
			$this->request->data = $this->Aviso->read(null, $id);
                        App::uses('Util', 'Vendor');
                        $this->request->data['Aviso']['data_inicio_inscricao_normal'] = Util::converteData($this->request->data['Aviso']['data_inicio_inscricao_normal']);            
                        $this->request->data['Aviso']['data_fim_inscricao_normal'] = Util::converteData($this->request->data['Aviso']['data_fim_inscricao_normal']);            
                        $this->request->data['Aviso']['data_inicio_inscricao_isento'] = Util::converteData($this->request->data['Aviso']['data_inicio_inscricao_isento']);            
                        $this->request->data['Aviso']['data_fim_inscricao_isento'] = Util::converteData($this->request->data['Aviso']['data_fim_inscricao_isento']);            
                        $this->request->data['Aviso']['data_exame'] = Util::converteData($this->request->data['Aviso']['data_exame']);            
                        $this->set(compact('campus'));
		}
	}

	public function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Id do Aviso inválido'), 'default', array('class' => 'error'));
			$this->redirect(array('action' => 'index'));
		}
		if ($this->Aviso->delete($id)) {
			$this->Session->setFlash(__('Aviso excluído com sucesso'), 'default', array('class' => 'success'));
			$this->redirect(array('action' => 'index'));
		}
	}

}
