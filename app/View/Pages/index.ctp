<?php #   ?>

<div class="index">

    <div class="row show-grid">
        <div class="span10">
            <h2>Instruções para as inscrições</h2>
            <ul>
                <li>Para se inscrever em algum exame/vestibular, efetue login no site, digitando seu CPF (apenas números) e sua senha nos campos acima;</li>
                <li>Caso ainda não possua uma senha, clique em "Cadastrar";</li>
                <li>Se seu CPF já estiver registrado em nossa base de dados e você não lembrar sua senha, clique em "Esqueci minha senha"</li>
            </ul>
          
            <!--
            <h2>Perguntas Frequentes</h2>

            <p>Clique aqui para ver as dúvidas mais frequentes</p>
            -->
        </div>
        
        <!--
        <div class="span4">
            <h2>Documentos</h2>
            <ul>
                <li>
                    <a href="">Concorrência do Exame de Seleção 2009.2</a>
                </li>
                <li>
                    <a href="">Concorrência do Exame de Seleção 2009.2</a>
                </li>
                <li>
                    <a href="">Concorrência do Exame de Seleção 2009.2</a>
                </li>

            </ul>
        </div>
            -->
    </div>
    
    <div class="row exames-andamento" >
        <h2>Exames em andamento</h2>
        
        <div class="exame" id="exame1">

            <div class="modal-header">
                <h3 class="">Exame de Seleção 2013 - Cursos Integrados</h3>
                <button class="btn btn-success">Inscrever-me!</button>
            </div>

            <div class="clear"></div>

            <ul>
                <li>Período das Inscrições para isentos: de 04/09/2012 a 12/09/2012</li>
                <li>Período das Inscrições para não isentos: de 04/09/2012 a 07/10/2012</li>
                <li>Data da Prova: 11/11/2012</li>
                <li>Taxa de Inscrição: R$ 15,00</li>
            </ul>


            <table class="table">
                <thead>
                    <tr>
                        <th>CURSO</th>
                        <th>HABILITAÇÃO</th>
                        <th>LOCAL</th>
                        <th>TURNO</th>
                        <th>VAGAS</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Controle e Processos Industriais</td>
                        <td>Eletrotécnica</td>
                        <td>MACEIÓ - AL</td>
                        <td>VESPERTINO</td>
                        <td>36</td>
                    </tr>
                    <tr>
                        <td>Controle e Processos Industriais</td>
                        <td>Eletrotécnica</td>
                        <td>MACEIÓ - AL</td>
                        <td>VESPERTINO</td>
                        <td>36</td>
                    </tr>
                    <tr>
                        <td>Controle e Processos Industriais</td>
                        <td>Eletrotécnica</td>
                        <td>MACEIÓ - AL</td>
                        <td>VESPERTINO</td>
                        <td>36</td>
                    </tr>
                    <tr>
                        <td>Controle e Processos Industriais</td>
                        <td>Eletrotécnica</td>
                        <td>MACEIÓ - AL</td>
                        <td>VESPERTINO</td>
                        <td>36</td>
                    </tr>
                </tbody>
            </table>
            <h5>Anexos</h5>
            <ul>
                <li><a href="#">Anexo 1</a></li>
                <li><a href="#">Anexo 2</a></li>
                <li><a href="#">Anexo 3</a></li>
                <li><a href="#">Anexo 4</a></li>
                <li><a href="#">Anexo 5</a></li>

            </ul>
            <h5>Avisos</h5>
            <ul>
                <li><a href="#">Aviso 1</a></li>
                <li><a href="#">Aviso 2</a></li>
                <li><a href="#">Aviso 3</a></li>
                <li><a href="#">Aviso 4</a></li>
                <li><a href="#">Aviso 5</a></li>

            </ul>
        </div>
        
        <div class="exame" id="exame2">

            <div class="modal-header">
                <h3 class="">Exame de Seleção 2013 - Cursos Integrados</h3>
                <button class="btn btn-success">Inscrever-me!</button>
            </div>

            <div class="clear"></div>

            <ul>
                <li>Período das Inscrições para isentos: de 04/09/2012 a 12/09/2012</li>
                <li>Período das Inscrições para não isentos: de 04/09/2012 a 07/10/2012</li>
                <li>Data da Prova: 11/11/2012</li>
                <li>Taxa de Inscrição: R$ 15,00</li>
            </ul>


            <table class="table">
                <thead>
                    <tr>
                        <th>CURSO</th>
                        <th>HABILITAÇÃO</th>
                        <th>LOCAL</th>
                        <th>TURNO</th>
                        <th>VAGAS</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Controle e Processos Industriais</td>
                        <td>Eletrotécnica</td>
                        <td>MACEIÓ - AL</td>
                        <td>VESPERTINO</td>
                        <td>36</td>
                    </tr>
                    <tr>
                        <td>Controle e Processos Industriais</td>
                        <td>Eletrotécnica</td>
                        <td>MACEIÓ - AL</td>
                        <td>VESPERTINO</td>
                        <td>36</td>
                    </tr>
                    <tr>
                        <td>Controle e Processos Industriais</td>
                        <td>Eletrotécnica</td>
                        <td>MACEIÓ - AL</td>
                        <td>VESPERTINO</td>
                        <td>36</td>
                    </tr>
                    <tr>
                        <td>Controle e Processos Industriais</td>
                        <td>Eletrotécnica</td>
                        <td>MACEIÓ - AL</td>
                        <td>VESPERTINO</td>
                        <td>36</td>
                    </tr>
                </tbody>
            </table>
            <h5>Anexos</h5>
            <ul>
                <li><a href="#">Anexo 1</a></li>
                <li><a href="#">Anexo 2</a></li>
                <li><a href="#">Anexo 3</a></li>
                <li><a href="#">Anexo 4</a></li>
                <li><a href="#">Anexo 5</a></li>

            </ul>
            <h5>Avisos</h5>
            <ul>
                <li><a href="#">Aviso 1</a></li>
                <li><a href="#">Aviso 2</a></li>
                <li><a href="#">Aviso 3</a></li>
                <li><a href="#">Aviso 4</a></li>
                <li><a href="#">Aviso 5</a></li>

            </ul>
        </div>

    </div>
</div>