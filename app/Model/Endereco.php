<?php
/**
 * Endereco
 *
 * PHP version 5
 *
 * @category Model
 * @package  CIATI
 * @version  1.0
 * @author   Uziel Barbosa
 * @license  http://www.opensource.org/licenses/mit-license.php The MIT License
 * @link     http://ciati.ifal.edu.br
 */
class Endereco extends AppModel {

    public $name = 'Endereco';

    public $validate = array(
        'endereco' => array(
            'rule' => 'notEmpty',
            'message' => 'Esse campo deve ser informado.',
        ),
        'numero' => array(
            'rule' => 'notEmpty',
            'message' => 'Esse campo deve ser informado.',
        ), 
        'cep' => array(
            'rule' => 'notEmpty',
            'message' => 'Esse campo deve ser informado.',
        ),
        'bairro' => array(
            'rule' => 'notEmpty',
            'message' => 'Esse campo deve ser informado.',
        ),
        'cidade' => array(
            'rule' => 'notEmpty',
            'message' => 'Esse campo deve ser informado.',
        ),
        'estado' => array(
            'rule' => 'notEmpty',
            'message' => 'Esse campo deve ser informado.',
        ),
    );
    
}

?>